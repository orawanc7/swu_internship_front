var gulp = require('gulp');
var minifyJS = require('gulp-uglify');
var rename = require('gulp-rename');

gulp.task('minFullCalendar', function () {
    return gulp
        .src('./resources/assets/libs/fullcalendar/dist/fullcalendar-th.js')
        .pipe(minifyJS())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./resources/assets/libs/fullcalendar/dist/'));
});

gulp.task('minCommon', function () {
    return gulp
        .src('./resources/assets/js/common.js')
        .pipe(minifyJS())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./resources/assets/js/'));
});



var jsFile = [
    './resources/assets/libs/jquery/dist/jquery.min.js',
    './resources/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js',
    './resources/assets/libs/select2/dist/js/select2.min.js',
    './resources/assets/libs/select2/dist/js/select2.full.min.js',
    './resources/assets/libs/fullcalendar/dist/fullcalendar-th.min.js',
    './resources/assets/libs/moment/min/moment.min.js',
    './resources/assets/libs/jquery-ui/jquery-ui.min.js',
    './resources/assets/libs/dropzone/dist/min/dropzone.min.js',
    './resources/assets/libs/jquery-ui/jquery-ui.min.js',
    './resources/assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js',
    './resources/assets/libs/summernote/dist/summernote-bs4.min.js',
    './resources/assets/libs/blockUI/jquery.blockUI.js',
    './resources/assets/libs/jquery-slimscroll/jquery.slimscroll.min.js',
    './resources/assets/libs/datatables.net/js/jquery.dataTables.min.js',
    './resources/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js',
    './resources/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js',
    './resources/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js',
    './resources/assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js',
    './resources/assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js',
    './resources/assets/libs/ModalWindowEffects/js/modalEffects.js',
    './resources/assets/libs/ModalWindowEffects/js/classie.js',
    './resources/assets/libs/jquery-treetable/jquery.treetable.js',
    './resources/assets/libs/cropperjs/dist/cropper.min.js',
    './resources/assets/libs/typeahead.js/dist/typeahead.bundle.min.js',
    './resources/assets/js/common.min.js',
];

var cssFile = [
    './resources/assets/libs/bootstrap/dist/css/bootstrap.min.css',
    './resources/assets/libs/select2/dist/css/select2.min.css',
    './resources/assets/libs/fullcalendar/dist/fullcalendar.min.css',
    './resources/assets/libs/dropzone/dist/min/dropzone.min.css',
    './resources/assets/libs/jquery-ui/themes/sunny/jquery-ui.min.css',
    './resources/assets/css/summernote/summernote-bs4.css',
    './resources/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css',
    './resources/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css',
    './resources/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css',
    './resources/assets/libs/ModalWindowEffects/css/component.css',
    './resources/assets/libs/jquery-treetable/css/jquery.treetable.css',
    './resources/assets/libs/jquery-treetable/css/jquery.treetable.theme.default.css',
    './resources/assets/libs/cropperjs/dist/cropper.min.css'
];

var fontFile = [
    './resources/assets/libs/summernote/dist/font/summernote.eot',
    './resources/assets/libs/summernote/dist/font/summernote.ttf',
    './resources/assets/libs/summernote/dist/font/summernote.woff'
];

gulp.task('libjs', function () {
    return gulp
        .src(jsFile)
        .pipe(gulp.dest('./wwwroot/js'));
});

gulp.task('libcss', function () {
    return gulp
        .src(cssFile)
        .pipe(gulp.dest('./wwwroot/css'));
});

gulp.task('libfont', function () {
    return gulp
        .src(fontFile)
        .pipe(gulp.dest('./wwwroot/font'));
});

gulp.task('liblocaleFullCalendar', function () {
    return gulp
        .src('./resources/assets/libs/fullcalendar/dist/locale/th.js')
        .pipe(gulp.dest('./wwwroot/js/locale_fullcalendar'));
});

gulp.task('all', ['minFullCalendar', 'minCommon', 'libjs', 'libcss', 'libfont', 'liblocaleFullCalendar']);