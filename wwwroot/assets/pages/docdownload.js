$(document).ready(function () {
    DocDownload.loadData();
});

var DocDownload = {
    loadData: function () {
        var apiUrl = $('#api-url').val();
        var lst = $('#lstDocDownload');
        $.ajax({
            type: "GET",
            url: $('#doc-download-url').val(),
            dataType: "json",
            success: function (response) {
                if (response.status) {
                    var docGroupDownloadId = "";
                    var ul;
                    $.each(response.data, function (index, item) {
                        var fileType = "";
                        var fileUrl = apiUrl + item.fileUrl;

                        if (item.fileExtension == "pdf") {
                            fileType = "<i class=\"far fa-file-pdf f-28 text-muted text-danger\"></i>";
                        } else if (item.fileExtension == "word") {
                            fileType = "<i class=\"far fa-file-word f-28 text-muted text-primary\"></i>";
                        }

                        if (item.docGroupDownloadId != docGroupDownloadId) {
                            lst.append("<h6 ><u>" + item.docGroupDownloadNameTh + "</u></h6>");

                            ul = $("<ul>", {
                                class: 'media-list p-0'
                            }).appendTo("#lstDocDownload");

                        }

                        var li = $("<li>", {
                            class: 'media d-flex m-b-15'
                        }).html(
                            "<div class=\"m-r-20 file-attach\">" +
                            fileType +
                            "</div>" +
                            "<div class=\"media-body\">" +
                            "<div class=\"m-b-5 text-secondary\">" + item.docDownloadNameTh + "</div>" +
                            "<div class=\"text-muted\">" +
                            "<span>ขนาด " + item.fileSize + "</span>" +
                            "</div>" +
                            "</div>" +
                            "<div class=\"float-right text-muted\">" +
                            "<a href=\"" + fileUrl + "\"><i class=\"fas fa-download f-18\"></i></a>" +
                            "</div>" +
                            "</li >"
                        );
                        ul.append(li);

                        docGroupDownloadId = item.docGroupDownloadId;
                    });
                }
            }
        });
    }
}