<?php

use eftec\bladeone;
use eftec\bladeone\BladeOneHtmlBootstrap;
use App\Classes\Response;

class myBlade extends  bladeone\BladeOne {
    use bladeone\BladeOneHtmlBootstrap;
}

function view($path, array $data = [])
{
    $view = __DIR__ . '/../../resources/views';
    $cache = __DIR__ . '/../../bootstrap/cache';
    
    $blade =  new myBlade($view,$cache);
    $blade->setBaseUrl(getenv('APP_URL'));     

    $blade->directive('userimagepath', function ($id) {
        return "<?php echo $id; ?>";
    });
    
    /*
    $blade->directiveRT('datetime', function ($format) {
        echo date($format);
    });    
    

    $blade->directive('datetime', function ($format) {
        return "<?php echo date(" . $format . "); ?>";
    });
    */
    
    echo $blade->run($path,$data);    
}

function callApi($method, $url, $data = false)
{
    $response = new Response();
    $url = routeApi($url);
    
    try{
        $ch = curl_init();
    
        switch ($method) {
            case 'POST':
                if ($data!==FALSE) {                    
                    
                    curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
                }
                break;
            case 'GET':                
                if ($data!==FALSE) {   
                    $url = $url . '/' . http_build_query($data);
                }
                break;
        }

        $verbose = fopen('php://temp', 'w+');

        curl_setopt( $ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER , true);     
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);   
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //curl_setopt($ch, CURLOPT_VERBOSE, true);        
        //curl_setopt($ch, CURLOPT_STDERR, $verbose);
        
        $result = curl_exec( $ch );
        

        // if ($result === FALSE) {
        //     printf("cUrl error (#%d): %s<br>\n", curl_errno($ch),
        //             htmlspecialchars(curl_error($ch)));
        // }
        // rewind($verbose);
        // $verboseLog = stream_get_contents($verbose);
            
        // echo "Verbose information: ", htmlspecialchars($verboseLog);
        
        
        if (curl_errno($ch)) {            
            $http_code  = curl_getinfo($ch,CURLINFO_HTTP_CODE);
            $response->setMessage('Error with ' . $http_code);
            
        } else {
            // echo 'Took ', $info['total_time'], ' seconds to send a request to ', $info['url'], "\n";

            $response->setData($result);
            $response->setStatus(true);
        }        

        curl_close($ch);        
    }catch(Exception $ex){      
        $response->setMessage($ex->getMessage());
    }
    
    return $response;
}

function routeApi ($url) {
    $strAppPath = getenv('API_URL');

    $strLastChr = substr($strAppPath,strlen($strAppPath),1);

        
    if ($strLastChr != "/") {
        $strAppPath .= "/";
    }    

    return  $strAppPath . $url;
}

function route($url)
{
    $strAppPath = getenv('APP_PATH');

    $strLastChr = substr($strAppPath,strlen($strAppPath),1);
        
    if ($strLastChr != "/") {
        $strAppPath .= "/";
    }

    return  $strAppPath . $url;
}

function icon($id)
{        
    $path = WWW_PATH . "images/users/icons/" . $id . ".png";

    if (file_exists($path)) {
        return route('images/users/icons/' . $id . '.png');        
    }

    return route('images/users/icons/blank.png');    
}

function profile($id)
{    
    $path = WWW_PATH . 'images/users/profiles/' . $id . '.png';

    if (file_exists($path)) {
        return route('images/users/profiles/' . $id . '.png');        
    }

    return route('images/users/profiles/blank.png');    
}