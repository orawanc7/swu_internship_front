<?php

/**
 *  Controller/Action 
 * */

$router->map('GET', '/', 'App\Controllers\IndexController@index', 'index');
$router->map('GET', '/login', 'App\Controllers\LoginController@index', 'login');
$router->map('GET', '/loginadmin', 'App\Controllers\LoginController@admin', 'loginadmin');
$router->map('GET', '/logout', 'App\Controllers\LoginController@logout', 'logout');
$router->map('POST', '/login/save', 'App\Controllers\LoginController@save', 'loginsave');

$router->map('GET', '/teacher', 'App\Controllers\TeacherController@index', 'teacher.index');
$router->map('GET', '/teacher/student/[a:itsStudentId]', 'App\Controllers\TeacherController@student', 'teacher.student');

$router->map('GET', '/student', 'App\Controllers\StudentController@index', 'student.index');
$router->map('GET', '/student/round/[a:roundId]', 'App\Controllers\StudentController@round', 'student.round');
$router->map('GET', '/student/mentor/[a:itsStudentId]', 'App\Controllers\StudentController@mentor', 'student.mentor');

$router->map('GET', '/profile', 'App\Controllers\ProfileController@index', 'profile.index');
$router->map('POST', '/profile/save', 'App\Controllers\ProfileController@save', 'profile.save');

$router->map('GET', '/document/[a:activityId]/[a:itsUserId]', 'App\Controllers\DocumentController@index', 'document.index');