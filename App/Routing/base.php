<?php

/**
 *  Default Controller/Action 
 * */

$router->map('GET', '/[a:controller]', 'App\Controllers\{controller}Controller@index', 'default-list');
$router->map('GET', '/[a:controller]/[a:method]/[i:id]?', 'App\Controllers\{controller}Controller@{method}', 'default-get');
$router->map('POST', '/[a:controller]/[a:method]', 'App\Controllers\{controller}Controller@{method}', 'default-post');
$router->map('DELETE', '/[a:controller]/[a:method]/[i:id]?', 'App\Controllers\{controller}Controller@{method}', 'default-delete');