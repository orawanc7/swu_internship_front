<?
namespace App\Classes;

trait UrlUtil
{
    public function exists($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($httpCode == 200) {
            return true;
        }
        return false;
    }

    public function download($url, $filePath)
    {
        try {

            if ($this->exists($url)) {
                $content = file_get_contents($url);

                if ($content) {
                    $fp = fopen($filePath, "w");
                    fwrite($fp, $content);
                    fclose($fp);
                    return true;
                }
            }
        } catch (Exception $e) { }

        return false;
    }
}
