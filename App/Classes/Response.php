<?php

namespace App\Classes;

class Response
{
    private $status;
    private $message;
    private $data;

    public function __construct() {
        $this->status = false;
    }
    
    public function setStatus($status) {
        $this->status = $status;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function getMessage() {
        return $this->message;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function getData() {
        return $this->data;
    }

    public function json() {
        $out = [
            'status' => $this->status,
            'message' => $this->message,
            'data' => $this->data
        ];

        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($out);        
    }    
}
