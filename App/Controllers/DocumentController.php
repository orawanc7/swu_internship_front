<?php

namespace App\Controllers;
use App\Repositories\BaseRepository;

class DocumentController extends BaseController {
    
    public function __construct() {
        if (!isset($_SESSION['PERSON_TYPE_ID'])) {
            header ('Location: ' . route(''));
            exit;
        }        
    }    

    public function index($param) {
        $activityId = $param['activityId'];
        $itsUserId = $param['itsUserId'];

        $result = callApi('GET','SchdActivity/get/' .  $activityId);
        
        
        if ($result->getStatus()) {            
            $output = json_decode($result->getData());                                               
            $data = $output->data;          
            
            // print_r($data);
            // exit;
            
            if ($data) {                
                
                //กรณีเป็น Owner
                if ($data->personTypeId==$_SESSION['PERSON_TYPE_ID']) {
                    $input = [
                        'docName' => $data->docNameTh,
                        'year' => $data->year,
                        'semCd' => $data->semCd,
                        'roundId' => $data->roundId,
                        'activityId' => $data->activityId,
                        'docId' => $data->docId,                        
                        'itsUserId' => $itsUserId,
                        'itsUserPersonTypeId' => $data->personTypeId,
                        'template' => $data->frmTemplate
                    ];

                    if ($data->docScreenFlag=="F") {
                        view('document.create.typeForm.index',$input);
                    } else if ($data->docTypeId==1) {
                        view('document.create.type1.index',$input);
                    }
                    
                } else {
                    //กรณีไม่ใช่ Owner
                    $input = [
                        'docName' => $data->docNameTh,
                        'year' => $data->year,
                        'semCd' => $data->semCd,
                        'roundId' => $data->roundId,
                        'activityId' => $data->activityId,
                        'docId' => $data->docId,
                        'itsUserId' => $itsUserId,
                        'personTypeId' => $data->personTypeId,
                    ];

                    if ($data->docTypeId==1) {
                        view('document.check.type1.index',$input);
                    }
                }
                
                return false;       
            }               
        }
                
        header('Location: ' . $_SESSION['URL']);
        exit;        
    }
}