<?php

namespace App\Controllers;
use App\Repositories\BaseRepository;

class StudentController extends BaseController {
    
    public function __construct() {
        parent::__construct();

        if (!isset($_SESSION['PERSON_TYPE_ID'])) {
            header ('Location: ' . route(''));
            exit;
        }

        if ($_SESSION['PERSON_TYPE_ID']!=getenv("PERSON_TYPE_STUDENT")) {
            header ('Location: ' . $_SESSION['URL']);
            exit;
        }
    }    

    public function index() {
        view('student.index');
    }

    public function round($param) {
        $url = 'SchdRound/get/' . $param['id'];
        $result = callApi('GET',$url);        
        if ($result->getData()) {            
            $output = json_decode($result->getData());                                   
            $data = $output->data;    
            
            if ($data!=null) {
                $_SESSION['ROUND_ID'] = $data->roundId;

                echo $this->getItsStudentInfo($_SESSION['ID'],$_SESSION['ROUND_ID']);
            }                        
        }            
        echo null;
    }

    private function getItsStudentInfo ($studentId,$roundId) {
        $data = [];
        $url = 'SchdStdInfo/getByStudentRound/' . $studentId . "/" . $roundId;        
        $result = callApi('GET',$url);        
        
        if ($result->getData()) {            
            return $result->getData();
        }         
        return null;
    }

    public function mentor($param) {
        $itsStudentId = $param['itsStudentId'];

        view('student.mentor.index',[
                'itsStudentId' => $itsStudentId
            ]);
    }
}    