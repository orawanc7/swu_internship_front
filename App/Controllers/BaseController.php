<?php

namespace App\Controllers;

class BaseController {
    public function __construct () {        
        if (!isset($_SESSION['ID'])) {
            header ('Location: ' . route(''));
            exit;
        }
    }    
}