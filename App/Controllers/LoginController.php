<?php

namespace App\Controllers;
use App\Repositories\BaseRepository;
use App\Classes\Response;

error_reporting(E_ERROR | E_PARSE);

class LoginController {
    public function index() {
        view('login');
    }    

    public function admin () {
        view('loginadmin');
    }

    public function logout()
    {
        session_destroy();
        header('Location: ' . route(''));
    }

    public function save() {
        $response = new Response();
    

        $input = [
            'userId' => $_POST['userId'],
            'userPassword' => $_POST['userPassword'],
            'admin' => isset($_POST['admin'])?$_POST['admin']:"N",
        ];

        $result = callApi('POST','Login',$input);
        
        if ($result->getStatus()) {            
            $output = json_decode($result->getData());                                   
            if ($output->status) {
                $data = $output->data;                

                $_SESSION['PERSON_TYPE_ID'] = $data->personTypeId;
                $_SESSION['ID'] = $data->id;
                $_SESSION['ITS_ID'] = $data->itsId;
                $_SESSION['TITLE'] = $data->title;
                $_SESSION['FIRST_NAME'] = $data->firstName;
                $_SESSION['LAST_NAME'] = $data->lastName;
                $_SESSION['NAME'] = $data->name;
                $_SESSION['MAJOR'] = $data->major;                
                $_SESSION['INDEPT'] = $data->indept;
                $_SESSION['DEPT'] = $data->dept;
                $_SESSION['TELEPHONE_NO'] = $data->telephoneNo;
                $_SESSION['MOBILE_NO'] = $data->mobileNo;
                $_SESSION['BUASRI_EMAIL'] = $data->buasriId;
                $_SESSION['GAFE_EMAIL'] = $data->gafeEmail;
                $_SESSION['OTHER_EMAIL'] = $data->otherEmail;
                $_SESSION['ADDRESS_DESC'] = $data->addressDesc;
                
                if ($data->personTypeId==getenv('PERSON_TYPE_STUDENT')) {            
                    $_SESSION['URL'] = route('student');
                    $response->setData(route('student'));
                    $response->setStatus(true);
                } else if ($data->personTypeId==getenv('PERSON_TYPE_TEACHER')) {
                    $_SESSION['URL'] = route('teacher');
                    $response->setData(route('teacher'));
                    $response->setStatus(true);
                } else if ($data->personTypeId==getenv('PERSON_TYPE_MENTOR')) {
                    $_SESSION['URL'] = route('teacher');
                    $response->setData(route('teacher'));
                    $response->setStatus(true);
                } else {   
                    // $response->setData(route('general'));
                    $response->setMessage("ไม่สามารถเข้าใช้ระบบได้");  
                }                            
            } else {
                $response->setMessage($output->message);                
            }
        }       
    
        
        return $response->json();
    }    
}