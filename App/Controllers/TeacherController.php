<?php

namespace App\Controllers;
use App\Repositories\BaseRepository;

class TeacherController extends BaseController  {
    public function __construct() {
        parent::__construct();

        if (!isset($_SESSION['PERSON_TYPE_ID'])) {
            header ('Location: ' . route(''));
            exit;
        }

        if ($_SESSION['PERSON_TYPE_ID']!=getenv("PERSON_TYPE_TEACHER")) {
            header ('Location: ' . $_SESSION['URL']);
            exit;
        }
        
    }
    public function index() {
        view('teacher.index');
    }

    public function student($param) {
        $itsStudentId = $param['itsStudentId'];

        view('teacher.student.index',[
                'itsStudentId' => $itsStudentId
            ]);
    }
}    