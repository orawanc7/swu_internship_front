<?php

namespace App\Controllers;

use App\Repositories\BaseRepository;

class IndexController
{
    public function index()
    {        
        view('index');
    }
    
}
