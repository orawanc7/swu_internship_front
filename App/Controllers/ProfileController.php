<?php

namespace App\Controllers;
use App\Repositories\BaseRepository;
use App\Classes\Response;

class ProfileController extends BaseController {
    
    public function __construct() {
        if (!isset($_SESSION['PERSON_TYPE_ID'])) {
            header ('Location: ' . route(''));
            exit;
        }
        
    }    

    public function index() {
        view('profile');
    }    

    public function save() {
        $response = new Response();

        $id = "";
        if ($_SESSION['PERSON_TYPE_ID']==getenv('PERSON_TYPE_STUDENT')) {         
            $id = $_SESSION['ID'];
        } else {
            $id = $_SESSION['ITS_ID'];
        }    

        $input = [
            'itsId' => $id,
            'otherEmail' => $_POST['otherEmail'],
            'telephoneNo' => $_POST['telephoneNo'],            
            'mobileNo' => $_POST['mobileNo'],
        ];

        $result = callApi('POST','Profile/save',$input);
                
        if ($result->getStatus()) {     
            $output = json_decode($result->getData());                                   
            if ($output->status) {                 
                
                $_SESSION['OTHER_EMAIL'] = $input['otherEmail'];
                $_SESSION['TELEPHONE_NO'] = $input['telephoneNo'];
                $_SESSION['MOBILE_NO'] = $input['mobileNo'];                
                
                $response->setStatus(true);
            } else {
                $response->setMessage($output->message);                
            }     
        }

        return $response->json();
    }
}    