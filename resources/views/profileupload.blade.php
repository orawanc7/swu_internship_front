<div class="modal fade" id="profile-upload" tabindex="-1" role="dialog" >
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><i class="feather icon-upload-cloud"></i> อัพโหลดรูปภาพใหม่</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
			</div>
			<div class="modal-body">
				<form id="profile-upload-form" method="post" action="{{ routeApi('Profile/upload') }}" class="dropzone dz-clickable">
					<input type="hidden" name="itsId" value="{{ $_SESSION['ITS_ID'] }}">
                    <div class="dz-default dz-message"><span>วางไฟล์ที่ต้องการที่นี่</span></div>
                </form>

                <div id="profile-edit" class="d-none">
                    <img id="imgProfileEdit" src="">
                </div>
			</div>			
			<div class="modal-footer">
                <button type="button" id="btnUpload" class="btn btn-sm btn-primary">เปลี่ยนรูปภาพ</button>                
            </div>
		</div>
	</div>
</div>