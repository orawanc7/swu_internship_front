<input type="hidden" name="doc-news-url" id="doc-news-url" value="{{ routeApi('News/getActive') }}">
<div class="card shadow mb-5 bg-white rounded">
    <div class="card-header">
        <h5><i class="fas fa-bullhorn f-18"></i> ข่าวประชาสัมพันธ์</h5>
        <div class="card-header-right">
            <div class="btn-group card-option">
                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="feather icon-more-horizontal"></i>
                </button>
                <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                    <li class="dropdown-item full-card"><a href="javascript:void();"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                    <li class="dropdown-item minimize-card"><a href="javascript:void();"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>                    
                </ul>
            </div>
        </div>
    </div>
    <div class="card-body task-attachment" id="lstNews">        
    </div>
</div>