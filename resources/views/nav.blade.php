<!-- [ navigation menu ] start -->
<nav class="pcoded-navbar theme-horizontal navbar-blue">
    <div class="navbar-wrapper">
        <div class="navbar-brand header-logo">
            <a href="index.html" class="b-brand">
                <img src="@relative('assets/images/logosmall.png')" alt="Swu Internship" class="img-fluid d-none d-md-block" />                    
                <img src="@relative('assets/images/logomobile.png')" alt="Swu Internship" class="img-fluid d-block d-md-none" />
                <img src="@relative('assets/images/logo-icon.svg')" alt="" class="logo-thumb images">	
			</a>
        </div>
        <div class="navbar-content sidenav-horizontal" id="layout-sidenav">
            <ul class="nav pcoded-inner-navbar sidenav-inner">
				<li class="nav-item pcoded-menu-caption">
					<label>Swu-Internship</label>
				</li>
                <li data-username="Home" class="nav-item"><a href="{{ route('') }}" class="nav-link"><span class="pcoded-micon"><i class="fa fa-home"></i></span><span class="pcoded-mtext">หน้าแรก</span></a></li>				
                <li data-username="Dashboard" class="nav-item"><a href="{{ $_SESSION['URL'] }}" class="nav-link"><span class="pcoded-micon"><i class="fa fa-tachometer-alt"></i></span><span class="pcoded-mtext">Dashboard</span></a></li>				
            </ul>
        </div>
    </div>
</nav>
<!-- [ navigation menu ] end -->