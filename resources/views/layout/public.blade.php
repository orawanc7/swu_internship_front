<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ระบบสารสนเทศเพื่อการบริหารจัดการในการปฏิบัติการสอนและฝึกประสบการณ์วิชาชีพครู InTernship System  - @yield('title')</title>
    <link rel="shortcut icon" href="@relative('assets/images/favicon.ico')" type="image/x-icon">
    <link rel="icon" href="@relative('assets/images/favicon.ico')" type="image/x-icon">
    
    <link rel="stylesheet" href="@relative('assets/fonts/fontawesome/css/fontawesome-all.min.css')">    
    <link rel="stylesheet" href="@relative('assets/plugins/animation/css/animate.min.css')">
    <link rel="stylesheet" href="@relative('assets/plugins/pnotify/css/pnotify.custom.min.css')">
    <link rel="stylesheet" href="@relative('assets/css/style.css')">
    
    @yield('style')
</head>
<body>    
    @yield('content')    
        
    <script src="@relative('assets/js/vendor-all.min.js')"></script>
    <script src="@relative('assets/plugins/bootstrap/js/bootstrap.min.js')"></script>
    <script src="@relative('assets/plugins/jquery-validation/js/jquery.validate.min.js')"></script>
    <script src="@relative('assets/plugins/pnotify/js/pnotify.custom.min.js')"></script>       
    <script src="@relative('assets/config/default.jquery-validation.js')"></script>
    <script src="@relative('assets/js/common.js')"></script>
    @yield('script')
</body>
</html>