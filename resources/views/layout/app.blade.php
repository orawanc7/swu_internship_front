<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ระบบสารสนเทศเพื่อการบริหารจัดการในการปฏิบัติการสอนและฝึกประสบการณ์วิชาชีพครู InTernship System - @yield('title')</title>
    <link rel="shortcut icon" href="@relative('assets/images/favicon.ico')" type="image/x-icon">
    <link rel="icon" href="@relative('assets/images/favicon.ico')" type="image/x-icon">
    
    <!-- fontawesome icon -->
    <link rel="stylesheet" href="@relative('assets/fonts/fontawesome/css/fontawesome-all.min.css')">
    <!-- animation css -->
    <link rel="stylesheet" href="@relative('assets/plugins/animation/css/animate.min.css')">
    <!-- prism css -->
    <link rel="stylesheet" href="@relative('assets/plugins/prism/css/prism.min.css')">
    
    <!-- vendor css -->    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mada:400,500,600">                
    <link rel="stylesheet" href="@relative('assets/plugins/pnotify/css/pnotify.custom.min.css')">
    <link rel="stylesheet" href="@relative('assets/plugins/select2/css/select2.min.css')">
    <link rel="stylesheet" href="@relative('assets/css/style.css')?{{ time() }}">
    
    @yield('style')
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">
    <input type="hidden" name="current-path" id="current-path" value="@relative('')">
    <input type="hidden" name="api-url" id="api-url" value="{{ routeApi('') }}">

    @include('nav')

    @include('header')
    
    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">

                    <!-- [ breadcrumb ] start -->
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    @yield('page-header')                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ breadcrumb ] end -->

                    <div class="main-body">
                        <div class="page-wrapper">

                            <!-- [ Main Content ] start -->
                            <div class="row">
                                <div class="col-sm-12">
                                    @yield('content')
                                </div>                                
                            </div>
                            <!-- [ Main Content ] end -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- [ Main Content ] end -->

    @yield('modal')

    <script src="@relative('assets/js/vendor-all.min.js')"></script>
    <script src="@relative('assets/plugins/bootstrap/js/bootstrap.min.js')"></script>
    <script src="@relative('assets/js/pcoded.min.js')"></script>
    <script src="@relative('assets/plugins/jquery-validation/js/jquery.validate.min.js')"></script>
    <script src="@relative('assets/config/default.jquery-validation.js')"></script>
    <script src="@relative('assets/plugins/pnotify/js/pnotify.custom.min.js')"></script>        
    <script src="@relative('assets/plugins/prism/js/prism.min.js')"></script>
    <script src="@relative('assets/plugins/inputmask/js/jquery.inputmask.bundle.min.js')"></script>    

    <script src="@relative('assets/plugins/sweetalert/js/sweetalert.min.js')"></script>
    <script src="@relative('assets/plugins/select2/js/select2.full.min.js')"></script>
    <script src="@relative('assets/js/common.js')"></script>
    
    @yield('script')
</body>
</html>