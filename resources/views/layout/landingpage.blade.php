<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ระบบสารสนเทศเพื่อการบริหารจัดการในการปฏิบัติการสอนและฝึกประสบการณ์วิชาชีพครู InTernship System - @yield('title')</title>
    <link rel="shortcut icon" href="@relative('assets/images/favicon.ico')" type="image/x-icon">
    <link rel="icon" href="@relative('assets/images/favicon.ico')" type="image/x-icon">
    
    <link rel="stylesheet" href="@relative('assets/landingpage/css/bootstrap.min.css')">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
    <link rel="stylesheet" href="@relative('assets/landingpage/css/animate.css')">
    <link rel="stylesheet" href="@relative('assets/landingpage/css/owl.carousel.css')">
    <link rel="stylesheet" href="@relative('assets/landingpage/css/owl.theme.css')">
    <link rel="stylesheet" href="@relative('assets/landingpage/css/magnific-popup.css')">
    <link rel="stylesheet" href="@relative('assets/landingpage/css/animsition.min.css')">
    <link rel="stylesheet" href="@relative('assets/landingpage/css/ionicons.min.css')">    

    <link rel="stylesheet" href="@relative('assets/landingpage/css/style.css')?<? echo time()?>">
        
    @yield('style')
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">
    <input type="hidden" name="current-path" id="current-path" value="@relative('')">

    @yield('content')    
    
    <script type="text/javascript" src="@relative('assets/landingpage/js/jquery-2.1.1.js')"></script>
    <script type="text/javascript" src="@relative('assets/landingpage/js/bootstrap.min.js')"></script>
    <script type="text/javascript" src="@relative('assets/landingpage/js/plugins.js')"></script>
    <script type="text/javascript" src="@relative('assets/landingpage/js/menu.js')"></script>
    <script type="text/javascript" src="@relative('assets/landingpage/js/custom.js')"></script>

    @yield('script')
</body>
</html>