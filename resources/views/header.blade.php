<!-- [ Header ] start -->
<header class="navbar pcoded-header navbar-expand-lg navbar-light">
    <div class="m-header">
        <a class="mobile-menu" id="mobile-collapse1" href="#!"><span></span></a>
        <a href="index.html" class="b-brand">
            <img src="@relative('assets/images/logosmall.png')" alt="Swu Internship" class="img-fluid d-none d-md-block" />                    
            <img src="@relative('assets/images/logomobile.png')" alt="Swu Internship" class="img-fluid d-block d-md-none" />
            <img src="@relative('assets/images/logo.png')" alt="" class="logo-thumb images">
        </a>
    </div>
    <a class="mobile-menu" id="mobile-header" href="#!">
        <i class="feather icon-more-horizontal"></i>
    </a>
    <div class="collapse navbar-collapse">
        <a href="#!" class="mob-toggler"></a>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                
            </li>            
        </ul>
        <ul class="navbar-nav ml-auto">
            <li>
                <div class="dropdown">
                    {{-- @include("notification") --}}
                </div>
            </li>
            <li>
                <div class="dropdown drp-user">
                    @include("user")
                </div>
            </li>
        </ul>
    </div>
</header>
<!-- [ Header ] end -->