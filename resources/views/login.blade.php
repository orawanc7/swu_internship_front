@extends('layout.public')

@section('title')
	เข้าสู่ระบบนิเทศเพื่อการศึกษา
@endsection

@section('content')

<!-- [ auth-signin ] start -->
<div class="auth-wrapper">
	<div class="auth-content container">
		<div class="card">
			<div class="row align-items-center">
				<div class="col-md-6">
					<div class="card-body">
						<form id="frmLogin" action="{{ route('login/save') }}" method="post">
							<div class="row align-items-center">
								<div class="col-8">
									<img src="@relative('assets/images/logo.png')" alt="" class="img-fluid mb-4">
								</div>
								<div class="col-4 text-right">									
									@if (getenv('HOST')) 
										<h3><i class="feather icon-server"></i> {{getenv('HOST')}}</h3>
									@endif									
								</div>
							</div>
							<h4 class="mb-3 f-w-400">เข้าสู่ระบบนิเทศเพื่อการศึกษา</h4>
							<div class="input-group mb-2">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="feather icon-mail"></i></span>
								</div>
								<input type="text" class="form-control" placeholder="Buasri Id" id="userId" name="userId" autofocus>
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="feather icon-lock"></i></span>
								</div>
								<input type="password" class="form-control" placeholder="Password" id="userPassword" name="userPassword">
							</div>
							
							<button class="btn btn-primary mb-4"><i class="feather icon-log-in"></i> Login</button>
						</form>

						<p class="text-center">
							Copyright © 2020 คณะศึกษาศาสตร์ มหาวิทยาลัยศรีนครินทรวิโรฒ
						</p>
					</div>
				</div>
				<div class="col-md-6 d-none d-md-block">
					<img src="@relative('assets/landingpage/images/banner.jpg')" alt="" class="img-fluid">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- [ auth-signin ] end -->

@endsection

@section('script')
<script>
    $(document).ready(function () {
		$('#frmLogin').submit(function (e) { 
			e.preventDefault();
			
			$.ajax({
				type: $(this).attr('method'),
				url: $(this).attr('action'),
				data: $(this).serialize(),
				dataType: "json",
				success: function (response) {					
					if (response.status) {
						location.href = response.data;
					} else {
						MessageNotify.error(response.message);						
					}
				}
			});
			
		});
	});
</script>
@endsection
