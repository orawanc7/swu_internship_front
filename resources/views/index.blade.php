@extends('layout.landingpage')

@section('content')
<div class="wrapper animsition" data-animsition-in-class="fade-in" data-animsition-in-duration="1000" data-animsition-out-class="fade-out" data-animsition-out-duration="1000">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <a class="navbar-brand page-scroll" href="#main">
                    <img src="@relative('assets/images/logosmall.png')" alt="Swu Internship" class="img-fluid d-none d-md-block" />                    
                    <img src="@relative('assets/images/logomobile.png')" alt="Swu Internship" class="img-fluid d-block d-md-none" />                    
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                    </ul>
                    <ul class="navbar-nav my-2 my-lg-0">
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#main">หน้าหลัก</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#services">การให้บริการ</a>
                        </li>                        
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#aboutus">เกี่ยวกับเรา</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="http://edu.swu.ac.th"  target="_blank">คณะศึกษาศาสตร์</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="https://www.swu.ac.th"  target="_blank">มศว</a>
                        </li>                                    
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <div class="main" id="main">
        <!-- Main Section-->
        <div class="hero-section app-hero">
            <div class="container">
                <div class="hero-content app-hero-content text-center">
                    <div class="row justify-content-md-center"  style="background-image: url(assets/landingpage/images/banner.jpg);background-size: cover;min-height:800px;">
                        <div class="col-md-12 text-left text-md-right">
                            <p class="wow fadeInUp" data-wow-delay="0">                                
                                &nbsp;<div class="line-it-button" data-lang="en" data-type="friend" data-lineid="@edswu" data-count="true" data-home="true" style="display: none;">
                                </div>&nbsp;
                                <script src="https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js" async="async" defer="defer"></script>
                            </p> 
                            <div class="d-none d-md-block">
                                {{-- <h1 class="wow fadeInUp text-right" data-wow-delay="0s">ระบบนิเทศเพื่อการศึกษา</h1>                         
                                <h1 class="wow fadeInUp text-right text-white" data-wow-delay="0.2s">Swu Internship</h1>         
                                &nbsp;<img src="@relative('assets/images/logo.png')" alt="Swu Internship"  class="img-fluid" data-wow-delay="0.4s" />&nbsp;
                                --}}
                            </div>  
                            
                            <br>
                            <p class="wow fadeInUp text-left text-md-right" data-wow-delay="0.2s">                                
                                &nbsp;<a class="btn btn-primary btn-action" data-wow-delay="0.10s" href="{{ route ('login') }}""><i class="ion-log-in"></i> เข้าสู่ระบบ</a>&nbsp;                                   
                            </p>                                                            
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
        <div class="services-section text-center" id="services">
            <!-- Services section (small) with icons -->
            <div class="container">
                <div class="row  justify-content-md-center">
                    <div class="col-md-12 text-center">
                        <div class="services">
                            <div class="row">
                                <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
                                    <div class="services-icon">
                                        <img class="icon-3" src="@relative("assets/landingpage/icons/teacher.svg")" height="60" width="60" alt="Student" />
                                    </div>
                                    <div class="services-description">
                                        <h1>อาจารย์</h1>
                                        <p>
                                            เชื่อมโยงข้อมูลระหว่างอาจารย์และนิสิต
                                        </p>
                                    </div>
                                </div>
                                <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.3s">
                                    <div class="services-icon">
                                        <img class="icon-3" src="@relative("assets/landingpage/icons/student.svg")" height="60" width="60" alt="Teacher" />
                                    </div>
                                    <div class="services-description">
                                        <h1>นิสิต</h1>
                                        <p>
                                            เชื่อมโยงข้อมูลระหว่างนิสิตและอาจารย์
                                        </p>
                                    </div>
                                </div>
                                <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.4s">
                                    <div class="services-icon">
                                        <img class="icon-3" src="@relative("assets/landingpage/icons/school.svg")" height="60" width="60" alt="School" />
                                    </div>
                                    <div class="services-description">
                                        <h1>สถานศึกษา</h1>
                                        <p>
                                            ข้อมูลในระบบสำหรับสถานศึกษา
                                        </p>
                                    </div>
                                </div>
                                <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.4s">
                                    <div class="services-icon">
                                        <img class="icon-3" src="@relative("assets/landingpage/icons/executive.svg")" height="60" width="60" alt="Executive" />
                                    </div>
                                    <div class="services-description">
                                        <h1>ผู้บริหาร</h1>
                                        <p>
                                            ข้อมูลในระบบสำหรับผู้บริหาร
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
                        
        <!-- Counter Section Ends -->
        <div class="features-section" id="aboutus">
            <!-- Feature section with flex layout -->
            <div class="f-left">
                <div class="left-content wow fadeInLeft" data-wow-delay="0s">
                    <h2 class="wow fadeInLeft" data-wow-delay="0.1s">เกี่ยวกับเรา</h2>
                    <p class="wow fadeInLeft" data-wow-delay="0.2s">
                        สำนักคอมพิวเตอร์ร่วมกับคณะศึกษาศาสตร์ พัฒนาระบบสารสนเทศเพื่อการบริหารจัดการในการปฏิบัติการสอนและฝึกประสบการณ์วิชาชีพครู สำหรับหลักสูตรการศึกษาบัณฑิต (กศ.บ. 5 ปี) มหาวิทยาลัยศรีนครินทรวิโรฒ เพื่อให้บริการข้อมูลสารสนเทศทางการศึกษาให้แก่นิสิตหลักสูตรการศึกษาบัณฑิต (กศ.บ. 5 ปี) มหาวิทยาลัยศรีนครินทรวิโรฒ
                    </p>
                    <a class="btn btn-primary btn-action btn-fill wow fadeInLeft" data-wow-delay="0.2s" href="https://docs.google.com/forms/d/e/1FAIpQLSe-_6jkd565T1GEaVrateGD5nwqN3xz7YXS7P2ePAvrY4W-BQ/viewform" target="_blank">แบบสอบถามความคิดเห็น</a>
                </div>
            </div>
            <div class="f-right">
            </div>
        </div>
                     
        <!-- Footer Section -->
        <div class="footer">
            <div class="container">
                <div class="col-md-12 text-center">                    
                    <div class="footer-text">
                        <p>
                            Copyright © 2020 คณะศึกษาศาสตร์ มหาวิทยาลัยศรีนครินทรวิโรฒ
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Scroll To Top -->
        <a id="back-top" class="back-to-top page-scroll" href="#main">
            <i class="ion-ios-arrow-thin-up"></i>
        </a>
        <!-- Scroll To Top Ends-->
    </div>
    <!-- Main Section -->
</div>
@endsection

@section('script')

@endsection
