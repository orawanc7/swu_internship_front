@extends('layout.app')

@section("style")
@endsection

@section("page-header")
<div class="page-header-title">
    <h5 class="m-b-10">ครู/อาจารย์พี่เลี้ยง</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('') }}"><i class="fa fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="{{ $_SESSION['URL'] }}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="javascript:void();">ครู/อาจารย์พี่เลี้ยง <span id="roundSemYear"></span></a></li>
</ul>
@endsection

@section('content')
<input type="hidden" name="itsStudentId" id="itsStudentId" value="{{ $itsStudentId }}">


<div class="card shadow mb-5 rounded card-border-c-blue">
    <div class="card-header">
        <span class="font-weight-bold"><i class="fas fa-landmark"></i> สถานที่ฝึกประสบการณ์</span>  <span id="schoolName"></span>
    </div>
    <div class="card-body" id="div-search">
        <div class="row">            
            <label for="personFnameTh" class="col-lg-2">ชื่อ-นามสกุล</label>
            <div class="col-lg-8">
                <input type="text" class="form-control typeahead" name="searchName" id="searchName" placeholder="ระบุเฉพาะชื่อ นามสกุล (ระบุอย่างน้อย 4 ตัวอักษร)" aria-label="ระบุเฉพาะชื่อ นามสกุล" autofocus autocomplete="off">                    
            </div>                
            <div class="col-lg-2">
                <button class="btn btn-primary" type="button" id="btnClear"><i class="fas fa-eraser"></i> ล้าง</button>                    
            </div>                        
        </div>     
    </div>   
</div>

<form action="{{ routeApi('SchdMentorStd/save') }}" method="post" id="frmMentor" >
    <div class="card shadow mb-5 rounded card-border-c-blue" id="div-mentor" style="display:none;">
        <input type="hidden" name="itsStudentId" value="{{ $itsStudentId }}">        
        <input type="hidden" name="deptCd" id="deptCd">
        <input type="hidden" name="mentorStdId" id="mentorStdId">
        <input type="hidden" name="itsPersonId" id="itsPersonId">
        <input type="hidden" name="itsPersonTypeId" id="itsPersonTypeId">        
        <input type="hidden" name="roundId" id="roundId">        
        <div class="card-block">                             
            <div class="form-group row">            
                <label for="prenameIntThCd" class="col-md-2 col-form-label form-control-label mandatory">คำนำหน้าชื่อ</label>
                <div class="col-md-4">                    
                    <select name="prenameIntThCd" id="prenameIntThCd" class="select2" required></select>
                </div>                    
            </div>     
            <div class="form-group row">            
                <label for="personFnameTh" class="col-md-2 col-form-label form-control-label mandatory">ชื่อ</label>
                <div class="col-md-4">
                    <div class="input-data">
                        <input type="text" id="personFnameTh" name="personFnameTh" class="form-control" placeholder="ชื่อ" required maxlength="50" autofocus>
                    </div>
                </div>
                <label for="personLnameTh" class="col-md-2 col-form-label form-control-label mandatory">นามสกุล</label>
                <div class="col-md-4">
                    <div class="input-data">
                        <input type="text" id="personLnameTh" name="personLnameTh" class="form-control" placeholder="นามสกุล" required maxlength="50">
                    </div>
                </div>                        
            </div>     
            <div class="form-group row">            
                <label for="mobileNo" class="col-md-2 col-form-label form-control-label">มือถือ</label>
                <div class="col-md-4">
                    <input type="text" id="mobileNo" name="mobileNo" class="form-control" placeholder="มือถือ" maxlength="50">
                </div>
                <label for="otherEmail" class="col-md-2 col-form-label form-control-label">อีเมล</label>
                <div class="col-md-4">
                    <div class="input-data">
                        <input type="text" id="otherEmail" name="otherEmail" class="form-control" placeholder="อีเมล" maxlength="50">
                    </div>
                </div>                        
            </div>     
        </div>   
        <div class="card-footer text-right">
            <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i> บันทึก</button>
            <button class="btn btn-sm btn-secondary" type="reset" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>
        </div>     
    </div>
</form>    


<div class="card shadow mb-5 rounded card-border-c-blue" id="div-mentor-list">   
    <div class="card-header">
        <i class="feather icon-users">รายชื่อครู/อาจารย์พี่เลี้ยงทั้งหมด</i>
    </div> 
    <div class="card-body">        
        <div class="table-responsive-sm">
            <table class="table table-sm table-striped table-hover" id="tbMentor">                           
                <thead>
                        <tr class="bg-primary">                
                            <th style="width:50%">ชื่อ-นามสกุล</th>
                            <th style="width:20%">มือถือ</th>
                            <th style="width:20%">อีเมล</th>                            
                            <th style="width:10%"></th>                
                        </tr>
                </thead>            
                <tbody>
                </tbody>
            </table>
        </div>
    </div>    
</div>
@endsection


@section('script')
@include('plugin.typeahead.js')
<script>        
    var nameIdMap = {};
    var notFound;
    $(document).ready(function () {
        var iconPath = "{{ routeApi('Profile/getImageIcon/') }}";            
        

        $('#prenameIntThCd').select2({
            width:'100%',        
        }); 

        $('#prenameIntThCd').change(function (e) { 
            e.preventDefault();
            
            if ($(this).val().length>0) {
                $(this).valid();
            }
        });

        $('#btnClear').click(function(){
            $('.typeahead').typeahead('val', '');
            $('#searchName').val("");
            $('#searchName').focus();
        });

        $('#btnCancel').click(function(){
            StudentMentor.clearScreen();

            $('#searchName').val("");

            $('#div-search').show();
            $('#div-mentor').hide();        

            $('#searchName').focus();
        });

        $('#div-search').on('click','#btnAdd',function() {    
            StudentMentor.setScreen(true);
                    
            $('#div-search').hide();
            $('#div-mentor').show();        

            $('#prenameIntThCd').focus();
        });

        $('#tbMentor').on("click", "a.update", function (e) {
            var mentorStdId = $(this).attr('data-id');
            $('#div-search').hide();
            $('#div-mentor').show();       
            StudentMentor.getMentor(mentorStdId);
        });

        $('#tbMentor').on("click", "a.delete", function () {
            
            var mentorStdId = $(this).attr('data-id');
            
            StudentMentor.deleteMentor(mentorStdId);
        });

        notFound = function (item) {
            var query = item.query;
            var arr = query.split(' ');
            if (arr.length>0) {
                $('#personFnameTh').val(arr[0]);

                if (arr.length>1) {
                    $('#personLnameTh').val(arr[1]);
                }
            }
            return '<div class="row"><div class="col-md-6">&nbsp;<i class="fa fa-times"></i> ไม่พบข้อมูลต้องการเพิ่มหรือไม่</div><div class="col-md-6 text-right"><button class="btn btn-sm btn-primary" id="btnAdd"><i class="fa fa-plus"></i> เพิ่ม</button></div></div></div>&nbsp;';
        }

        $('#searchName').on('typeahead:selected', function(evt, item) {      
            $('#div-mentor').show();          
            StudentMentor.getPerson(nameIdMap[item]);        
        });

        $('#searchName').typeahead({
            hint: true,
            highlight: true,
            minLength: 4
            },
            {
                source: function ( query, syncResults, asyncResults) {
                    var deptCd = $('#deptCd').val();
                    $.get( '{{ routeApi('Person/getExternalByDeptAndName') }}', { 'deptCd': deptCd, 'name' : query }, function ( response ) {
                        var resultList = [];

                        if (response.status) {
                            
                            $.each(response.data, function (idx, item) { 
                                var aItem = {id: item.itsPersonId , name: item.personFnameTh + " " + item.personLnameTh};
                                resultList.push(aItem);
                            });
                        }

                        return asyncResults(getOptionsFromJson(resultList));
                    } );
                },
                updater: function (item) {                
                    return item;
                },
                templates: {
                    notFound: notFound
                }              
            }        
        );   

        $('#frmMentor').validate({
            rules : {
                'otherEmail' : {
                    'email' : true
                }
            }
        });

        $('#frmMentor').submit(function (e) {
            e.preventDefault();

            if ($(this).valid()) {            
                $('#prenameIntThCd').prop('disabled',false)
                $.ajax({
                    type: 'post',
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function (response) { 
                        if (response.status) {      
                            MessageNotify.saveSucess();                 
                            $('#div-mentor').hide();
                            $('#div-search').show();
                            StudentMentor.clearScreen();                
                            StudentMentor.loadMentor();
                        } else {
                            MessageNotify.error(message);
                        }
                    }
                });
            }        
        });

        StudentMentor.loadPrename();
        StudentMentor.getInfo();     

        setTimeout(function() {
            $('#searchName').focus();
        },200);           
    });

    function getOptionsFromJson(json) {
        $.each(json, function (i, v) {
            nameIdMap[v.name] = v.id;
        });

        return $.map(json, function (n, i) {
            return n.name;
        });
    }

    var StudentMentor = {    
        loadPrename: function() {
            $.ajax({
                type: "GET",
                url: "{{routeApi('WSPrename')}}",
                dataType: "json",
                success: function (response) {
                    if (response.data) {
                        var data = response.data;

                        $('#prenameIntThCd').append($('<option>', {
                                value: '',
                                text: 'กรุณาเลือกรายการ'
                        }));
                        $.each(data, function (idx, item) {
                            $('#prenameIntThCd').append($('<option>', {
                                value: item.prenameCd,
                                text: item.prenameLnameTh
                            }));
                        });
                    }
                }
            });            
        }, 
        getInfo: function(iconPath) {            
            $.ajax({
                type: "get",
                url: "{{routeApi('SchdStdInfo/get/')}}" + $('#itsStudentId').val(),                
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        var data = response.data;

                        $('#roundId').val(data.roundId);                        
                        $('#roundSemYear').html("ประจำภาค"+data.semCd + "/" + data.year);                                                                        
                        $('#schoolName').html(data.schoolNameTh);
                        $('#deptCd').val(data.schoolId);                              

                        StudentMentor.loadMentor();
                    }
                }
            });
        },

        getPerson : function(itsPersonId) {       
            StudentMentor.clearScreen();
            $.ajax({
                type: "GET",
                url: "{{routeApi('Person/get')}}/" + itsPersonId,                        
                dataType: "json",
                success: function (response) {
                    if (response.status) {                                                                    
                        var data = response.data;
                        if (data) {
                            $('#div-search').hide();

                            $('#itsPersonId').val(data.itsPersonId);
                            $('#personFnameTh').val(data.personFnameTh);
                            $('#personLnameTh').val(data.personLnameTh);
                            $('#prenameIntThCd').val(data.prenameCd).trigger('change');
                            $('#mobileNo').val(data.mobileNo);
                            $('#otherEmail').val(data.otherEmail);
                                                                                                                                        
                            StudentMentor.getPersonType(data.itsPersonId);                    

                            $('#mobileNo').focus();                    
                        }
                    }
                }
            });
        },
        getPersonType : function(itsPersonId) {        
            
            $.ajax({
                type: "GET",
                url: "{{routeApi('ItsPersonType/getMentorByItsPersonId')}}/" + itsPersonId,                        
                dataType: "json",
                success: function (response) {                
                    if (response.status) {                                        
                        var data = response.data;
                        if (data) {
                            $('#itsPersonTypeId').val(data.itsPersonTypeId);
                            StudentMentor.getMentorByItsStudentItsPersonType(data.itsPersonTypeId);
                        }
                    } else {
                        StudentMentor.setScreen(false);                        
                    }                
                }
            });
        },
        loadMentor: function() {        
            $('#tbMentor tbody').empty();
            
            $.ajax({
                type: "get",
                url: "{{ routeApi('SchdMentorStd/getPersonByItsStudent') }}/" + $('#itsStudentId').val(),
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        var seq = 0;
                        var data = response.data;
                        $.each(data, function (idx, item) {                            
                            $('#tbMentor tbody').append(
                                '<tr>' +
                                '<td>' + item.prenameSnameTh + item.personFnameTh + " " + item.personLnameTh + '</td>' +
                                '<td>' + ((item.mobileNo==null)?"":item.mobileNo) + '</td>' +
                                '<td>' + ((item.otherEmail==null)?"":item.otherEmail) + '</td>' +
                                '<td class="text-right">' +
                                '<span class="dtr-data">' +
                                '<a class="btn btn-sm btn-primary text-white update" data-id="' + item.mentorStdId + '"><i class="fas fa-user-edit text-white"></i> แก้ไข</a>&nbsp;' +
                                '<a class="btn btn-sm btn-danger text-white delete" data-id="' + item.mentorStdId + '"><i class="fas fa-trash-alt text-white"></i> ลบ</a> ' +
                                '</span>' +
                                '</td>' +
                                '</tr>'
                            );                            
                        });
                    }                    
                }
            });
        },    
        getMentorByItsStudentItsPersonType : function(itsPersonTypeId) {                
            $.ajax({
                type: "GET",
                url: "{{ routeApi('SchdMentorStd/getByItsStudentItsPersonType')}}/" + $('#itsStudentId').val() + "/" + itsPersonTypeId,                        
                dataType: "json",
                success: function (response) {                         
                    if (response.status) {                                        
                        var data = response.data;
                        if (data) {                            
                            $('#mentorStdId').val(data.mentorStdId);
                        }
                    }
                    StudentMentor.setScreen(false);                    
                }
            });
        },
        getMentor : function(mentorStdId) {                    
            $.ajax({
                type: "GET",
                url: "{{routeApi('SchdMentorStd/get')}}/" + mentorStdId,                        
                dataType: "json",
                success: function (response) {
                    if (response.status) {                                        
                        var data = response.data;
                                                                                                
                        $('#mentorStdId').val(data.mentorStdId);
                        $('#itsPersonId').val(data.itsPersonId);
                        $('#itsPersonTypeId').val(data.itsPersonTypeId);
                        $('#personFnameTh').val(data.personFnameTh);
                        $('#personLnameTh').val(data.personLnameTh);                        
                        $('#prenameIntThCd').val(data.prenameCd).trigger('change');
                        $('#mobileNo').val(data.mobileNo);
                        $('#otherEmail').val(data.otherEmail);                                                                                
                        $('#mobileNo').focus();                    
                    }

                    StudentMentor.setScreen(false);                    
                }
            });
        },
        deleteMentor: function (mentorStdId) {
            
            $.ajax({                
                type: "POST",
                crossDomain:true,
                url: "{{routeApi('SchdMentorStd/delete')}}/"  + mentorStdId,                
                dataType: "json",
                success: function (response) {
                    StudentMentor.clearScreen();
                    StudentMentor.loadMentor();
                }
            });
        },
        setScreen: function(status) {
            if (status) {
                $('#personFnameTh').prop('readOnly',false);
                $('#personLnameTh').prop('readOnly',false);
                $('#prenameIntThCd').prop('disabled',false);
            } else {
                $('#personFnameTh').prop('readOnly',true);
                $('#personLnameTh').prop('readOnly',true);
                $('#prenameIntThCd').prop('disabled',true);

            }
        },
        clearScreen: function () {
            $('#frmMentor')[0].reset();

            $('#itsPersonId').val("");
            $('#itsPersonTypeId').val("");
            $('#mentorStdId').val("");
            $('#prenameIntThCd').val("").trigger('change');

            $('.typeahead').typeahead('val', '');        
        }
    }
</script>
@endsection