<div class="card shadow mb-5 rounded card-border-c-blue">
    <div class="card-header">
        <h5><i class="feather icon-file-text"></i> <i class="feather icon-star-on"></i> เอกสารปลายภาค</h5>        
        <div class="card-header-right">
            <div class="btn-group card-option">
                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="feather icon-more-horizontal"></i>
                </button>
                <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                    <li class="dropdown-item full-card"><a href="javascript:void();"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                    <li class="dropdown-item minimize-card"><a href="javascript:void();"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>                    
                </ul>
            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <div class="docfinal-scroll" style="height:400px;position:relative;">
                <table class="table table-hover m-0" id="tblDocFinal">
                    <thead>
                        <tr>                        
                            <th class="text-center" style="width:30%">สถานะ</th>                                                                               
                            <th class="text-left" style="width:15%">เอกสาร</th>                        
                            <th class="text-center" style="width:20%">ช่วงวันที่</th>                        
                        </tr>
                    </thead>
                    <tbody>                    
                    </tbody>                
                </table>            
            </div>
        </div>        
    </div>    
</div>