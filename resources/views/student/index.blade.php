@extends('layout.app')

@section("page-header")
@endsection

@section('content')

<input type="hidden" name="itsStudentId" id="itsStudentId">
<input type="hidden" name="activitySetNo" id="activitySetNo">
<input type="hidden" name="personTypeId" id="personTypeId" value="{{ getenv('PERSON_TYPE_STUDENT') }}">

<div class="row">        
    <div class="col-xl-8">        
        <div class="row">
            <div class="col-xl-12">
                <div class="input-group mb-3">
                    <div class="input-group-append">
                        <span class="input-group-text">ภาค/ปีการศึกษา</span>
                      </div>
                      <select name="roundId" id="roundId" class="form-control"></select>                    
                </div>
            </div>            
        </div>

        <div class="row mb-2">
            <div class="col-xl-12">
                <img class="icon-3" src="assets/landingpage/icons/school.svg" height="24" width="24" alt="School">
                &nbsp;&nbsp;สถานที่ฝึกปฏิบัติประสบการณ์ : <span id="schoolName"></span>
            </div>            
        </div>

        <div class="row">
            <div class="col-xl-12">
                @include("student.personrelate")
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12">
                @include("student.seminar")
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                @include("student.doc")
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                @include("student.docfinal")
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                @include("student.docviewer")
            </div>
        </div>
    </div>
    <div class="col-xl-4">
        <div class="row">
            <div class="col-xl-12">
                @include("share.download")
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12">
                @include("share.news")
            </div>
        </div>
    </div>
</div>    
                        
@endsection

@section('script')
@include('plugin.datetime.js')
<script src="@relative('assets/pages/docdownload.js')?time()"></script>
<script>
    $(document).ready(function () {        
        
        $('#roundId').change(function (e) { 
            Student.changeRound($(this).val());
        });

        $('#btnMentor').click(function (e) { 
            e.preventDefault();
                        

            location.href = "{{route ('student/mentor')}}" + '/' + $('#itsStudentId').val();
        });

        $("#tblSeminar tbody").on("click", "button.semimar-check-in", function(e) {
            var activityId = $(this).attr('data-activity-id');
            Student.seminarCheck('IN',activityId);
        });

        $("#tblSeminar tbody").on("click", "button.semimar-check-out", function(e) {
            var activityId = $(this).attr('data-activity-id');
            Student.seminarCheck('OUT',activityId);
        });

        $('#tblDoc,tblDocFinal').on("click", "button.doc-activity", function () {
            
            var activityId = $(this).attr('data-activity-id');
            
            location.href = "{{ route('document') }}/" + activityId + '/' + $('#itsStudentId').val();
        });

        var pxd = new PerfectScrollbar('.doc-scroll', {
            wheelSpeed: .5,
            swipeEasing: 0,
            wheelPropagation: 1,
            minScrollbarLength: 40,
        });

        var pxdf = new PerfectScrollbar('.docfinal-scroll', {
            wheelSpeed: .5,
            swipeEasing: 0,
            wheelPropagation: 1,
            minScrollbarLength: 40,
        });
        
        Student.loadRound();
        
    });

    var Student = {
        loadRound: function() {
            $.ajax({
                type: "get",
                url: "{{routeApi('SchdRound/getAll')}}",     
                crossDomain:true,           
                dataType: "json",
                success: function (response) {                    
                    if (response.data) {
                        var data = response.data;
                        
                        $.each(data, function (idx, item) {
                            $('#roundId').append($('<option>', { 
                                value: item.roundId,
                                text : item.roundNameTh 
                            }));
                        });              
                        $('#roundId').trigger('change');
                    }
                }
            });
        },
        changeRound: function(roundId) {
            $.ajax({
                type: "get",
                url: "{{route('Student/round')}}/" + roundId,     
                crossDomain:true,           
                dataType: "json",
                success: function (response) {                    
                    if (response) {
                        $('#itsStudentId').val(response.itsStudentId);
                        $('#activitySetNo').val(response.activitySetNo);
                        
                        $('#schoolName').html(response.schoolNameTh);

                        Student.loadPersonRelate();
                        Student.loadSeminar(roundId);
                        Student.loadDoc(roundId);      
                        Student.loadDocViewer(roundId);
                    }
                }
            });
        },
        loadSeminar: function(roundId) {
            var itsStudentId = $('#itsStudentId').val();
            var personTypeId = $('#personTypeId').val();

            $('#tblSeminar tbody').empty();
            $.ajax({
                type: "get",
                url: "{{routeApi('SchdSeminar/getSeminarByRoundAttendee')}}",     
                crossDomain:true,           
                dataType: "json",
                data : {
                    'roundId' : roundId,
                    'activitySetNo' : $('#activitySetNo').val(),
                    'personTypeId' : personTypeId,
                    'attendeeId' : itsStudentId
                },
                success: function (response) {                    
                    if (response.data) {
                        var data = response.data;
                        
                        $.each(data, function (idx, item) {
                            var start = "";
                            var end = "";
                            var rangeDate = "";
                            var startDate = "";
                            var endDate = "";
                            var seminarStartDate = "";
                            var seminarEndDate = "";

                            if (item.startDate!=null) {
                                startDate = moment(item.startDate.date).format("DD/MM/") + (parseInt(moment(item.startDate.date).format("YYYY"))+543);                                
                            }
                            if (item.endDate!=null) {
                                endDate = moment(item.endDate.date).format("DD/MM/") + (parseInt(moment(item.endDate.date).format("YYYY")) + 543);                                
                            }

                            if (startDate==endDate) {
                                rangeDate = startDate;
                            } else {
                                rangeDate = startDate + "-" + endDate;
                            }

                            
                            if (item.seminarStartDate!=null) {
                                seminarStartDate = moment(item.seminarStartDate.date).format("DD/MM/") +
                                                            (parseInt(moment(item.seminarStartDate.date).format("YYYY"))+543) + " " +
                                                            moment(item.seminarStartDate.date).format("HH:mm");                                                            
                                start = seminarStartDate;
                            } else {
                                start = "<button class=\"btn btn-sm btn-primary semimar-check-in\" data-activity-id=\""+ item.activityId + "\"><i class=\"feather icon-map-pin\"></i> Check In</button>";
                            }

                            if (item.seminarEndDate!=null) {                                
                                seminarEndDate = moment(item.seminarEndDate.date).format("DD/MM/") +
                                                            (parseInt(moment(item.seminarEndDate.date).format("YYYY"))+543) + " " +
                                                            moment(item.seminarEndDate.date).format("HH:mm");                             
                                end = seminarEndDate;
                            } else {                  
                                if (item.seminarStartDate!=null) {              
                                    end = "<button class=\"btn btn-sm btn-success semimar-check-out\" data-activity-id=\""+ item.activityId + "\"><i class=\"feather icon-log-out\"></i> Check Out</button>";
                                } else {
                                    end = "<button class=\"btn btn-sm btn-success semimar-check-out disabled\" data-activity-id=\""+ item.activityId + "\"><i class=\"feather icon-log-out\"></i> Check Out</button>";
                                }
                            }                           

                            $('#tblSeminar tbody').append(
                                "<tr>" +
                                    "<td class=\"text-center\">" + start + "</td>" +
                                    "<td class=\"text-center\">" + end + "</td>" +
                                    "<td>" + item.activityNameTh + "</td>" +
                                    "<td>" + rangeDate + "</td>" +                                                                        
                                "</tr>"
                            );
                        });
                    }
                }
            });
        },
        loadDoc(roundId) {
            $('#tblDoc tbody').empty();
            $('#tblDocFinal tbody').empty();
            $.ajax({
                type: "get",
                url: "{{routeApi('SchdDocStatus/getByRoundActivitySetNoOwner')}}",     
                crossDomain:true,           
                dataType: "json",
                data : {
                    'roundId' : roundId,
                    'activitySetNo' : $('#activitySetNo').val(),
                    'personTypeId' : $('#personTypeId').val(),
                    'itsUserId' : $('#itsStudentId').val()
                },
                success: function (response) {       
                    
                    if (response.status) {
                        var data = response.data;                  
                        var status = "";                        
                        var startDate = "";
                        var endDate = "";
                        var docDate = "";
                        $.each(data, function (idx, item) {     

                            if (item.startDate!=null) {
                                startDate = moment(item.startDate.date).format("DD/MM/") + (parseInt(moment(item.startDate.date).format("YYYY"))+543);                                
                            }
                            if (item.endDate!=null) {
                                endDate = moment(item.endDate.date).format("DD/MM/") + (parseInt(moment(item.endDate.date).format("YYYY")) + 543);                                
                            }

                            if (startDate==endDate) {
                                docDate = startDate;
                            } else {
                                docDate = startDate + "-" + endDate;
                            }

                            var url = "{{ route('document/')}}" + item.activityId;
                            var statusWait = "<span class=\"badge badge-primary\" style=\"font-size:100%\">" + item.waitCount + "</span>&nbsp;";
                            var statusReturn = "<span class=\"badge badge-warning\" style=\"font-size:100%\">" + item.returnCount + "</span>&nbsp;";
                            var statusPass = "<span class=\"badge badge-success\" style=\"font-size:100%\">" + item.passCount + "</span>&nbsp;";                           

                            $('#tblDoc tbody').append(
                                "<tr>" +                                    
                                    "<td class=\"text-center align-middle\">" + 
                                        "<button class=\"btn btn-sm btn-info doc-activity\" data-activity-id=\""+ item.activityId + "\"><i class=\"feather icon-info\"></i> รายละเอียด</button>" + 
                                        statusWait + statusReturn + statusPass +
                                    "</td>" +                                    
                                    "<td class=\"align-middle\">" + item.docNameTh + "</td>" +
                                    "<td class=\"text-center align-middle\">" + docDate + "</td>" +                                                                                                            
                                "</tr>"                                
                            );     
                                    
                            if (item.docFinalFlag=="Y") {
                                $('#tblDocFinal tbody').append(
                                    "<tr>" +                                    
                                        "<td class=\"text-center align-middle\">" + 
                                            "<button class=\"btn btn-sm btn-info doc-activity\" data-activity-id=\""+ item.activityId + "\"><i class=\"feather icon-info\"></i> รายละเอียด</button>" + 
                                            statusWait + statusReturn + statusPass +
                                        "</td>" +
                                        "<td class=\"align-middle\">" + item.docNameTh + "</td>" +
                                        "<td class=\"text-center align-middle\">" + docDate + "</td>" +                                                                                                            
                                    "</tr>"                                
                                );  
                            }
                        });                           
                    }
                }
            });
        }, 
        loadDocViewer(roundId) {
            $('#tblDocViewer tbody').empty();
            $.ajax({
                type: "get",
                url: "{{routeApi('SchdDocStatus/getByRoundNotOwnerCanViewer')}}",     
                crossDomain:true,           
                dataType: "json",
                data : {
                    'roundId' : roundId,
                    'activitySetNo' : $('#activitySetNo').val(),
                    'personTypeId' : $('#personTypeId').val(),
                    'itsUserId' : $('#itsStudentId').val()
                },
                success: function (response) {       
                    
                    if (response.status) {
                        var data = response.data;                  
                        var action = "";
                        var statusDate = "";                        
                        $.each(data, function (idx, item) {     
                            
                            if (statusDate.length>0) {
                                action = "<button class=\"btn btn-sm btn-info\"><i class=\"fas fa-eye text-white\"></i>ดูข้อมูล</button>";
                            } else {
                                action = "<button class=\"btn btn-sm btn-info\" disabled><i class=\"fas fa-eye text-white\"></i>ดูข้อมูล</button>";
                            }

                            $('#tblDocViewer tbody').append(
                                "<tr>" +                                    
                                    "<td class=\"text-center align-middle\">" + action + "</td>" +
                                    "<td class=\"text-center align-middle\">" + statusDate + "</td>" +
                                    "<td class=\"align-middle\">" + item.docNameTh + "</td>" +                                    
                                "</tr>"                                
                            );                            
                        });                           
                    }
                }
            });
        }, 
        loadPersonRelate() {
            var iconPath = "{{ routeApi('Profile/getImageIcon/') }}";    
            var itsStudentId = $('#itsStudentId').val();

            $('#person-relate').empty();
            $.ajax({
                type: "get",
                url: "{{routeApi('SchdStdInfo/getRelateByItsStudent/')}}" + itsStudentId,     
                crossDomain:true,           
                dataType: "json",
                success: function (response) {                                        
                    if (response.data) {
                        var data = response.data;       
                        
                        $.each(data, function (idx, item) {       
                            
                            $('#person-relate').append(
                                "<div class=\"align-middle m-b-25\">" +     
                                    "<img src=\"" +   iconPath + item.itsId  +   "\" alt=\"user image\" class=\"img-radius align-top m-r-15\" width=\"80px\">" + 
                                    "<div class=\"d-inline-block\">" +
                                        "<h6>" + item.itsName + "</h6>" +                                         
                                        "<p class=\"m-b-0\">" + item.personTypeNameTh +"</p>" +                                        
                                    "</div>" +
                                "</div>"
                            );                            
                            
                        });                        
                    }
                }
            });
        },
        seminarCheck: function(type,activityId) {
            var roundId = $('#roundId').val();
            $.ajax({
                type: "post",
                url: "{{routeApi('SchdSeminar/saveCheck')}}" ,     
                data : {
                    'activityId' : activityId,
                    'personTypeId' : $('#personTypeId').val(),
                    'attendeeId' : $('#itsStudentId').val(),
                    'checkType' : type
                },
                crossDomain:true,           
                dataType: "json",
                success: function (response) {                    
                    if (response) {
                        Student.loadSeminar(roundId);
                    }
                }
            });
        },        
    }
</script>
@endsection
