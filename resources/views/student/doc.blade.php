<div class="card shadow mb-5 rounded card-border-c-blue">
    <div class="card-header">
        <h5><i class="feather icon-file-text"></i> เอกสาร</h5>        
        <div class="card-header-right">
            <div class="btn-group card-option">
                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="feather icon-more-horizontal"></i>
                </button>
                <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                    <li class="dropdown-item full-card"><a href="javascript:void();"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                    <li class="dropdown-item minimize-card"><a href="javascript:void();"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>                    
                </ul>
            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="row">
            <div class="col-lg-4">
                <div class="card prod-p-card bg-c-blue">
                    <div class="card-body">
                        <div class="row align-items-center m-b-5">
                            <div class="col">
                                <h6 class="m-b-5 text-white">จำนวนที่รอตรวจ</h6>
                                <h3 class="m-b-0 text-white" id="doc-wait-count">0</h3>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-hourglass-half text-c-blue f-18"></i>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="card prod-p-card bg-c-yellow">
                    <div class="card-body">
                        <div class="row align-items-center m-b-5">
                            <div class="col">
                                <h6 class="m-b-5 text-white">จำนวนส่งกลับ</h6>
                                <h3 class="m-b-0 text-white" id="doc-return-count">0</h3>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-reply text-c-yellow f-18"></i>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="card prod-p-card bg-c-green">
                    <div class="card-body">
                        <div class="row align-items-center m-b-5">
                            <div class="col">
                                <h6 class="m-b-5 text-white">จำนวนที่ผ่าน</h6>
                                <h3 class="m-b-0 text-white" id="doc-pass-count">0</h3>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-check-double text-c-green f-18"></i>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>            
        </div>
        
        <div class="table-responsive">
            <div class="doc-scroll" style="height:400px;position:relative;">
                <table class="table table-hover m-0" id="tblDoc">
                    <thead>
                        <tr>                        
                            <th class="text-center" style="width:15%">สถานะ</th>                            
                            <th class="text-left" style="width:75%">เอกสาร</th>                        
                            <th class="text-center" style="width:20%">ช่วงวันที่</th>                        
                        </tr>
                    </thead>
                    <tbody>                    
                    </tbody>                
                </table>            
            </div>
        </div>        
    </div>    
</div>