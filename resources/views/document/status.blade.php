<div class="modal fade" id="doctype-activity-status-form" tabindex="-1" role="dialog" aria-labelledby="modalActivityStatus" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">        
        <form action="{{routeApi('SchdDocStatus/send')}}" method="POST" id="frmDocStatus">            
            <input type="hidden" name="activityId">
            <input type="hidden" name="docId">
            <input type="hidden" name="refDocId">
            <input type="hidden" name="itsUserId">
            <input type="hidden" name="personTypeId">            
            <div class="modal-content" id="doctype-activity-status">                
                <div class="modal-body">                    
                    <div class="form-group row sender-remark">
                        <div class="col-md-2"><label for="doctype-activity-status-remark">หมายเหตุ</label></div>
                        <div class="col-md-10">
                            <textarea id="doctype-activity-status-remark" name="remark" cols="30" rows="3" class="form-control" autofocus></textarea>
                        </div>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> ยกเลิก</button>
                    <button type="submit" class="btn btn-sm btn-info" id="btnActivitySend"><i class="fas fa-paper-plane text-white"></i> ส่งงาน</button>
                </div>
            </div>
        </form>
    </div>
</div>