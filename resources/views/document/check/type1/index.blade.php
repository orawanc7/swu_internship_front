@extends('layout.app')

@section('style')
@endsection

@section("page-header")
<div class="page-header-title">
    <h5 class="m-b-10">{{ $docName }} ประจำภาค {{ $semCd }} / {{ $year }}</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('') }}"><i class="fa fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="{{ $_SESSION['URL'] }}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="javascript:void();">{{ $docName }}</a></li>
</ul>
@endsection

@section('content')

<div class="row">
    <div class="col-lg-4">
        @include('document.translist')
    </div>
    <div class="col-lg-8">

    </div>
</div>
    

@endsection

@section('script')
<script>

</script>
@endsection