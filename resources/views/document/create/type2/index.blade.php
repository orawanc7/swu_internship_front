@extends('layout.app')

@section('style')
@include('plugin.dropzone.css')
@endsection

@section("page-header")
<div class="page-header-title">
    <h5 class="m-b-10">{{ $docName }} ประจำภาค {{ $semCd }} / {{ $year }}</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('') }}"><i class="fa fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="{{ $_SESSION['URL'] }}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="javascript:void();">{{ $docName }}</a></li>
</ul>
@endsection

@section('content')

<form action="{{ routeApi ('SchdStdTeach/save') }}" id="frmDocTypeTeach" method="post">    
    @include('document.create.type1.save')    

    @include('document.upload')
</form>
    
@include('document.create.type2.all')

@include('document.status')     
@endsection

@section('script')
@include('plugin.dropzone.js')
<script>
Dropzone.autoDiscover = false;  
$(document).ready(function () {    
    
    $(".time").inputmask(
        { mask: "99:99" }
    );

    $("input[name=minutesQty]").inputmask(
        {
            mask: "9[9]",greeday: false
        }
    );

    $('#tbDocTypeCourse').on("click", "button.update", function (e) {
        var stdCourseId = $(this).attr('data-id');
        var canedit = $(this).attr('data-canedit');
        
        DocTypeCourse.getData(stdCourseId, canedit);
    });

    $('#tbDocTypeCourse').on("click", "button.delete", function () {
        var stdCourseId = $(this).attr('data-id');
        DocTypeCourse.deleteData(stdCourseId);
    });

    $('#tbDocTypeCourse').on("click", "button.send", function () {
        var stdCourseId = $(this).attr('data-id');
        
        DocStatus.send(stdCourseId);
    });

    $('#btnCancel').click(function () {
        DocTypeCourse.clearScreen();
        $('#frmDocTypeCourse input[name=itsCourseCd]').focus();
    });

    $('#frmDocTypeCourse').validate({
        rules: {
            'minutesQty': {
                required: true,
                digits: true
            }
        }
    });

    $('#frmDocTypeCourse').submit(function (e) {
        e.preventDefault();

        if ($(this).valid()) {            
            $.ajax({
                type: 'post',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json",
                success: function (response) {                    
                    DocTypeCourse.clearScreen();
                    
                    if (response.status) {
                        if (response.data) {
                            DocTypeCourse.getData(response.data, "true");
                        }
                    } else {
                        MessageNotify.error(response.message);
                    }
                    DocTypeCourse.loadData();
                }
            });
        }
    });

    $('#btnFileUpload').click(function() {
        DocUpload.open();
    });

    DocStatus.setup();
    DocUpload.setup();
    DocTypeCourse.loadData();
});

var DocStatus = {
    setup: function() {
        $('#doctype-activity-status-form').on('shown.bs.modal', function() {
            $('#doctype-activity-status-remark').focus();
        });

        $('#frmDocStatus').submit(function (e) {
            e.preventDefault();

            if ($(this).valid()) {            
                $.ajax({
                    type: 'post',
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function (response) {                    
                        DocTypeCourse.clearScreen();
                        
                        if (response.status) {
                            if (response.data) {                                
                                
                                $('#doctype-activity-status-form').modal('hide');
                                DocTypeCourse.loadData();
                            }
                        } else {
                            MessageNotify.error(response.message);
                        }                        
                    }
                });
            }
        });
    },
    send : function (refDocId) {        
        
        $('#frmDocStatus input[name=refDocId]').val(refDocId);
        $('#frmDocStatus input[name=activityId]').val($('#activityId').val());
        $('#frmDocStatus input[name=docId]').val($('#docId').val());
        $('#frmDocStatus input[name=itsUserId]').val($('#itsStudentId').val());
        $('#frmDocStatus input[name=personTypeId]').val($('#personTypeId').val());
        $('#doctype-activity-status-form').modal('show');
    }
}

var DocUpload = {
    setup: function() {            
        $('#upload-form').dropzone({
            acceptedFiles: ".pdf,.doc,.docx,.xls,.xlsx,.ppt,.pptx,image/png,image/jpg,image/jpeg",
            maxFiles : 1,
            maxFilesize: 20,
            addRemoveLinks: true,
            autoProcessQueue: false,
            uploadMultiple: true,
            paramName: 'file',
            init: function() {
                this.on("addedfile", function (file) {
                    $('#btnUpload').removeClass('d-none');
                });

                this.on("removedfile", function (file) {
                    if (this.files.length==0) {
                        $('#btnUpload').addClass('d-none');
                    }
                });

                this.on("maxfilesexceeded", function() {
                    if (this.files[1]!=null){                    
                        this.removeFile(this.files[1]);
                    }
                });                  
                
                this.on("success", function(file, responseText) {
                    this.removeAllFiles();
                    $('#upload').modal('hide');

                    Activity.getUploadList();
                });

                var dropZone = this;
                $('#btnUpload').on('click',function() {                
                    dropZone.processQueue();                    
                });             
            },               
            error: function (file, message, xhr) {
                if (xhr == null) this.removeFile(file); // perhaps not remove on xhr errors
                alert(message);  
            },
        });  
    } , 
    open: function () {        
        $('#upload').modal('show');
    }
}

var DocTypeCourse = {
    loadData: function () {
        var url = "{{ routeApi('SchdStdCourse/getByItsStudent') }}" + '/' + "{{ $itsUserId }}";
        
        $('#tbDocTypeCourse tbody').empty();
        
        $.ajax({
            type: "get",
            url: url,
            dataType: "json",
            success: function (response) {
                if (response.status) {
                    var data = response.data;
                    $.each(data, function (idx, item) {
                        var buttonWithStatus = "";
                        var canEdit = true;
                        var disabled = "";
                        
                        if (item.docStatusId==null) {
                            buttonWithStatus = '<button class="btn btn-sm btn-info text-white send" data-id="' + item.stdCourseId + '"><i class="fas fa-paper-plane text-white"></i> ส่งงาน</button> ';
                        } else if (item.statusFlag=="W") {
                            buttonWithStatus = '<button class="btn btn-sm btn-primary text-white wait" disabled data-id="' + item.stdCourseId + '"><i class="fas fa-hourglass-half text-white"></i> รอตรวจ</button> ';
                            disabled = " disabled ";
                            canEdit = false;
                        } else if (item.statusFlag=="R") {
                            buttonWithStatus = '<button class="btn btn-sm btn-warning text-white send" data-id="' + item.stdCourseId + '"><i class="fas fa-reply text-white"></i> ส่งใหม่</button> ';                            
                        } else if (item.statusFlag=="Y") {
                            buttonWithStatus = '<button class="btn btn-sm btn-warning text-white" disabled data-id="' + item.stdCourseId + '"><i class="fas fa-check-double text-white"></i> รอตรวจ</button> ';
                            canEdit = false;
                            disabled = " disabled ";
                        }

                        var edit = "";
                        if (canEdit) {
                            edit = '<i class="fas fa-edit text-white"></i>&nbsp;&nbsp;&nbsp;แก้ไข';
                        } else {
                            edit = '<i class="fas fa-eye text-white"></i>ดูข้อมูล';
                        }

                        $('#tbDocTypeCourse tbody').append(
                            '<tr>' +
                            '<td>' + item.itsCourseCd + '</td>' +
                            '<td>' + item.itsCourseName + '</td>' +
                            '<td>' + item.levelClass + '</td>' +
                            '<td class="text-left text-sm-right">' + item.creditAmt + '</td>' +
                            '<td class="text-left text-sm-right">' + item.minutesQty + '</td>' +
                            '<td class="text-left text-sm-right">' + item.hourAmt + '</td>' +
                            '<td class="text-left text-sm-right">' +
                            '<button class="btn btn-sm btn-primary text-white update" data-id="' + item.stdCourseId + '" data-canedit="' + canEdit + '">' + edit + '</button>&nbsp;' +
                            '<button class="btn btn-sm btn-danger text-white delete" data-id="' + item.stdCourseId + '"' +  disabled  + '><i class="fas fa-trash-alt text-white"></i> ลบ</button> '  +
                            buttonWithStatus +
                            '</td>' +
                            '</tr>'
                        );
                    });                    
                }
            }
        });
    },
    getData: function (stdCourseId, canedit) {
        var url = "{{ routeApi('SchdStdCourse/get') }}" + '/' + stdCourseId;
        $.ajax({
            type: "get",
            url: url,
            dataType: "json",
            success: function (response) {
                if (response.status) {
                    var data = response.data;
                    $('#frmDocTypeCourse input[name=stdCourseId]').val(data.stdCourseId);
                    $('#frmDocTypeCourse input[name=itsCourseCd]').val(data.itsCourseCd);
                    $('#frmDocTypeCourse input[name=itsCourseName]').val(data.itsCourseName);
                    $('#frmDocTypeCourse input[name=levelClass]').val(data.levelClass);
                    $('#frmDocTypeCourse input[name=creditAmt]').val(data.creditAmt);
                    $('#frmDocTypeCourse input[name=minutesQty]').val(data.minutesQty);
                    $('#frmDocTypeCourse input[name=hourAmt]').val(data.hourAmt);
                    $('#frmDocTypeCourse input[name=remark]').val(data.remark);

                    $('#frmDocTypeCourse input[name=itsCourseCd]').select();

                    
                    if (canedit == "true") {                        
                        DocTypeCourse.setScreen(true);
                    } else {                        
                        DocTypeCourse.setScreen(false);
                    }                    
                }
            }
        });
    },

    deleteData: function (stdCourseId) {      
        var url = "{{ routeApi('SchdStdCourse/save') }}" + '/' + stdCourseId;  
        $.ajax({
            type: "delete",
            url: url,
            dataType: "json",
            success: function (response) {
                DocTypeCourse.clearScreen();
                DocTypeCourse.loadData();
            }
        });
    },

    clearScreen: function () {
        $('#frmDocTypeCourse')[0].reset();
        $('#frmDocTypeCourse').validate().resetForm();

        $('#frmDocTypeCourse button[type=submit]').prop('disabled', false);        

        $('#frmDocTypeCourse input[name=stdCourseId]').val("");

        DocTypeCourse.setScreen(true);
    },

    setScreen: function (enabled) {
        if (enabled) {
            $('#frmDocTypeCourse input').removeAttr('readonly');
            $('#frmDocTypeCourse button[type=submit]').prop('disabled', false);
            $('#btnFileUpload').prop('disabled', false);
        } else {
            $('#frmDocTypeCourse input').attr('readonly', 'readonly');
            $('#frmDocTypeCourse button[type=submit]').prop('disabled', true);
            $('#btnFileUpload').prop('disabled', true);
        }
    }
}    
</script>
@endsection