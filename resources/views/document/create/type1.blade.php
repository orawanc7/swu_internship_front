@extends('layout.app')

@section('style')
@include('plugin.smart-wizard.css')
@endsection

@section("page-header")
<div class="page-header-title">
    <h5 class="m-b-10">{{ $docName }} ประจำภาค {{ $semCd }} / {{ $year }}</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('') }}"><i class="fa fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="{{ $_SESSION['URL'] }}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="javascript:void();">{{ $docName }}</a></li>
</ul>
@endsection

@section('content')

<form action="{{ routeApi ('SchdStdCourse/save') }}" id="frmDocTypeCourse" method="post">
    <input type="hidden" id="activityId" value="{{ $activityId }}">
    <input type="hidden" id="itsStudentId" value="{{ $itsUserId }}">
    <input type="hidden" id="stdCourseId">    
    <div class="card shadow mb-5 rounded card-border-c-blue" id="div-doctype-course-save">    
        <div class="card-body">                               
            <div class="form-group row">            
                <label for="itsCourseCd" class="col-md-2 col-form-label form-control-label mandatory">รหัสวิชา</label>
                <div class="col-md-4">
                    <div class="input-data">
                        <input type="text" name="itsCourseCd" class="form-control" placeholder="รหัสวิชาที่" required maxlength="15" autofocus>
                    </div>
                </div>
                <label for="itsCourseName" class="col-md-2 col-form-label form-control-label mandatory">ชื่อรายวิชา</label>
                <div class="col-md-4">
                    <div class="input-data">
                        <input type="text" name="itsCourseName" class="form-control" placeholder="ชื่อรายวิชา" required maxlength="100">
                    </div>
                </div>                        
            </div>            
            <div class="form-group row">
                <label for="levelClass" class="col-md-2 col-form-label form-control-label mandatory">ระดับชั้น</label>
                <div class="col-md-4">
                    <div class="input-data">
                        <input type="text" name="levelClass" class="form-control" placeholder="ระดับชั้น" required maxlength="100">
                    </div>
                </div>
                <label for="creditAmt" class="col-md-2 col-offset-sm-2 col-form-label form-control-label mandatory">หน่วยกิต</label>
                <div class="col-md-4">
                    <div class="input-data">
                        <input type="text" name="creditAmt" class="form-control text-right" required>
                    </div>
                </div>                               
            </div>
            <div class="form-group row">            
                <label for="minutesQty" class="col-md-2 col-form-label form-control-label mandatory">นาทีต่อ 1 คาบ</label>
                <div class="col-md-4">
                    <div class="input-data">
                        <input type="text" name="minutesQty" class="form-control text-right" required data-error="">
                    </div>
                </div>     
                <label for="hourAmt" class="col-md-2 col-form-label form-control-label mandatory">ชั่วโมงทั้งวิชา</label>
                <div class="col-md-4">                
                    <div class="input-data">
                        <input type="text" name="hourAmt" class="form-control text-right" required maxlength="30">                
                    </div>
                </div>                           
            </div>
            <div class="form-group row">            
                <label for="stdRemark" class="col-md-2 col-form-label form-control-label">หมายเหตุ</label>
                <div class="col-md-10">                
                    <input type="text" name="stdRemark" class="form-control" maxlength="200">                
                </div>                           
            </div>
                        
        </div>        
        <div class="card-footer text-right">
            <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i> บันทึก</button>
            <button class="btn btn-sm btn-secondary" type="reset" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>
        </div>
    </div>            
    </form>                                                            
    
    
    <div class="card shadow mb-5 rounded card-border-c-blue " id="div-doctype-course-list"> 
        <div class="card-header">
            <i class="fas fa-list"> รายวิชาทั้งหมด</i>
        </div>   
        <div class="card-body">                                            
            <div class="table-responsive">
                <table class="table table-hover m-0" id="tbDocTypeCourse">                           
                    <thead>
                            <tr class="bg-primary">                
                                <th style="width:10%">รหัสวิชา</th>
                                <th style="width:24%">ชื่อรายวิชา</th>
                                <th style="width:14%">ระดับชั้น</th>
                                <th style="width:12%" class="text-left text-sm-right">หน่วยกิต</th>
                                <th style="width:12%" class="text-left text-sm-right">นาทีต่อ 1 คาบ</th>
                                <th style="width:12%" class="text-left text-sm-right">ชั่วโมงทั้งวิชา</th>                
                                <th style="width:16%" class="text-left text-sm-right"></th>                                                              
                            </tr>
                    </thead>            
                    <tbody>
                    </tbody>
                </table>     
            </div>   
        </div>    
    </div>        
@endsection

@section('script')
@include('plugin.smart-wizard.js')
<script>
$(document).ready(function () {    
    $(".time").inputmask(
        { mask: "99:99" }
    );

    $("input[name=minutesQty]").inputmask(
        {
            mask: "99"
        }
    );

    $('#tbDocTypeCourse').on("click", "button.update", function (e) {
        var stdCourseId = $(this).attr('data-id');
        var canedit = $(this).attr('data-canedit');
        
        DocTypeCourse.getData(stdCourseId, canedit);
    });

    $('#tbDocTypeCourse').on("click", "button.delete", function () {
        var stdCourseId = $(this).attr('data-id');
        DocTypeCourse.deleteData(stdCourseId);
    });

    $('#btnCancel').click(function () {
        DocTypeCourse.clearScreen();
        $('#frmDocTypeCourse input[name=itsCourseCd]').focus();
    });

    $('#frmDocTypeCourse').validate({
        rules: {
            'minutesQty': {
                required: true,
                digits: true
            }
        }
    });

    $('#frmDocTypeCourse').submit(function (e) {
        e.preventDefault();

        if ($(this).valid()) {            
            $.ajax({
                type: 'post',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json",
                success: function (response) {                    
                    DocTypeCourse.clearScreen();

                    if (response) {
                        if (response.stdCourseId) {
                            DocTypeCourse.getData(response.stdCourseId, "true");
                        }
                    }
                    DocTypeCourse.loadData();
                }
            });
        }
    });

    DocTypeCourse.loadData();
});

var DocTypeCourse = {
    loadData: function () {
        var url = "{{ routeApi('SchdStdCourse/getByItsStudent') }}" + '/' + "{{ $itsUserId }}";
        
        $('#tbDocTypeCourse tbody').empty();
        
        $.ajax({
            type: "get",
            url: url,
            dataType: "json",
            success: function (response) {
                if (response.status) {
                    var data = response.data;
                    $.each(data, function (idx, item) {
                        $('#tbDocTypeCourse tbody').append(
                            '<tr>' +
                            '<td>' + item.itsCourseCd + '</td>' +
                            '<td>' + item.itsCourseName + '</td>' +
                            '<td>' + item.levelClass + '</td>' +
                            '<td class="text-left text-sm-right">' + item.creditAmt + '</td>' +
                            '<td class="text-left text-sm-right">' + item.minutesQty + '</td>' +
                            '<td class="text-left text-sm-right">' + item.hourAmt + '</td>' +
                            '<td class="text-left text-sm-right">' +
                            '<button class="btn btn-sm btn-primary text-white update" data-id="' + item.stdCourseId + '"><i class="fas fa-edit text-white"></i> แก้ไข</button>&nbsp;' +
                            '<button class="btn btn-sm btn-danger text-white delete" data-id="' + item.stdCourseId + '"><i class="fas fa-trash-alt text-white"></i> ลบ</button> '  +
                            '</td>' +
                            '</tr>'
                        );
                    });                    
                }
            }
        });
    },
    getData: function (stdCourseId, canedit) {
        var url = "{{ routeApi('SchdStdCourse/get') }}" + '/' + stdCourseId;
        $.ajax({
            type: "get",
            url: url,
            dataType: "json",
            success: function (response) {
                if (response.status) {
                    var data = response.data;
                    $('#frmDocTypeCourse input[name=stdCourseId]').val(data.stdCourseId);
                    $('#frmDocTypeCourse input[name=itsCourseCd]').val(data.itsCourseCd);
                    $('#frmDocTypeCourse input[name=itsCourseName]').val(data.itsCourseName);
                    $('#frmDocTypeCourse input[name=levelClass]').val(data.levelClass);
                    $('#frmDocTypeCourse input[name=creditAmt]').val(data.creditAmt);
                    $('#frmDocTypeCourse input[name=minutesQty]').val(data.minutesQty);
                    $('#frmDocTypeCourse input[name=hourAmt]').val(data.hourAmt);
                    $('#frmDocTypeCourse input[name=stdRemark]').val(data.stdRemark);

                    $('#frmDocTypeCourse input[name=itsCourseCd]').select();

                    
                    if (canedit == "true") {
                        $('#frmDocTypeCourse button[type=submit]').prop('disabled', false);
                    } else {
                        $('#frmDocTypeCourse button[type=submit]').prop('disabled', true);
                    }                    
                }
            }
        });
    },

    // deleteData: function (stdCourseId) {        
    //     $.ajax({
    //         type: "delete",
    //         url: $('#doctype-course-delete-url').val() + "/" + stdCourseId,
    //         dataType: "json",
    //         success: function (response) {
    //             current.clearScreen();
    //             current.loadData();
    //         }
    //     });
    // },

    clearScreen: function () {
        $('#frmDocTypeCourse')[0].reset();
        $('#frmDocTypeCourse').validate().resetForm();

        $('#frmDocTypeCourse button[type=submit]').prop('disabled', false);

        $('#div-doctype-upload').addClass('d-none');

        $('#frmDocTypeCourse input[name=stdCourseId]').val("");

    }
}    
</script>
@endsection