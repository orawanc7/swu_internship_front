@extends('layout.app')

@section('style')
@include('plugin.dropzone.css')
@endsection

@section("page-header")
<div class="page-header-title">
    <h5 class="m-b-10">{{ $docName }} ประจำภาค {{ $semCd }} / {{ $year }}</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('') }}"><i class="fa fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="{{ $_SESSION['URL'] }}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="javascript:void();">{{ $docName }}</a></li>
</ul>
@endsection

@section('content')

<div class="card shadow mb-5 rounded card-border-c-blue " id="div-doctype-form"> 
    <div class="card-body">      
        
        <form id="fb-render"></form>

        <form id="upload-form" action="{{ routeApi ('SchdFrm/save') }}" class="dropzone dz-clickable" method="post"> 
                    
            <input type="hidden" name="frmHdrId" id="frmHdrId">
            <input type="hidden" name="roundId" id="roundId" value="{{ $roundId }}">
            <input type="hidden" name="activityId" id="activityId" value="{{ $activityId }}">
            <input type="hidden" name="docId" id="docId" value="{{ $docId }}">
            <input type="hidden" name="itsUserId" id="itsUserId" value="{{ $itsUserId }}">
            <input type="hidden" name="personTypeId" id="personTypeId" value="{{ $personTypeId }}">                                  
                        

            <div class="dz-default dz-message">
                <span>ลากไฟล์มาตรงนี้ หรือ คลิกเลือกไฟล์ <br>
                    เฉพาะไฟล์ PDF,WORD,EXCEL,POWERPOINT,IMAGE (PNG),IMAGE (JPG)                    
                </span> <br>และขนาดไฟล์ไม่เกิน 20MB เท่านั้น
            </div>
        </form>        
    </div>
    <div class="card-footer text-right">
        <button class="btn btn-sm btn-primary d-none" type="button"  id="btnUpload"><i class="fa fa-save"></i> บันทึก</button>        
    </div>
</div>    

@include('document.uploadlist')

@include('document.status')     
@endsection

@section('script')
@include('plugin.jqueryui.js')
@include('plugin.formbuilder.js')
@include('plugin.dropzone.js')
<script>
Dropzone.autoDiscover = false;  
$(document).ready(function () {    
    $('#btnFileUpload').click(function() {
        DocAttach.open();
    });    
              

    DocAttach.init();
    DocForm.init();
    DocForm.load();
});

var DocAttach = {
    init: function() {            
        $('#upload-form').dropzone({
            acceptedFiles: ".pdf,.doc,.docx,.xls,.xlsx,.ppt,.pptx,image/png,image/jpg,image/jpeg",
            maxFiles : 1,
            maxFilesize: 20,
            addRemoveLinks: true,
            autoProcessQueue: false,
            uploadMultiple: true,
            paramName: 'file',
            init: function() {                
                this.on("addedfile", function (file) {
                    $('#btnUpload').removeClass('d-none');
                });

                this.on("removedfile", function (file) {
                    if (this.files.length==0) {
                        $('#btnUpload').addClass('d-none');
                    }
                });

                this.on("maxfilesexceeded", function() {
                    if (this.files[1]!=null){                    
                        this.removeFile(this.files[1]);
                    }
                });                  
                
                this.on("success", function(file, responseText) {
                    this.removeAllFiles();
                    $('#btnUpload').addClass('d-none');

                    if (responseText.status) {                        
                        $('#frmHdrId').val(responseText.data);
                    }

                    DocAttach.list( $('#frmHdrId').val());
                });

                var dropZone = this;
                $('#btnUpload').on('click',function() {                
                    dropZone.processQueue();                    
                });             
            },               
            error: function (file, message, xhr) {
                if (xhr == null) this.removeFile(file); // perhaps not remove on xhr errors
                alert(message);  
            },
        });  
    },
    list : function(refDocId) {        
        $('#tblAttachList tbody').empty();
        $.ajax({
            type: "get",
            url: "{{routeApi('SchdAttach/getByRefDocId')}}/"  + refDocId ,     
            crossDomain:true,           
            dataType: "json",
            success: function (response) {                    
                if (response.data) {
                    var data = response.data;
                    
                    $.each(data, function (idx, item) {            
                        var urlDownload = "{{routeApi('SchdAttach/download')}}/"  + item.attachId;
                        var urlDelete = "javascript:DocAttach.delete(" + item.attachId + ");";
                        var downloadFile = "<a class=\"btn btn-sm btn-info\" href=\"" + urlDownload + "\"><i class=\"fa fa-folder-open\"></i>เปิดไฟล์</a>";
                        var deleteFile = "<a class=\"btn btn-sm btn-danger\" href=\"" + urlDelete + "\"><i class=\"fa fa fa-trash\"></i>ลบไฟล์</a>";
                        $('#tblAttachList tbody').append(
                            "<tr>" +
                                "<td class=\"text-left\">" + item.fileName + "</td>" +
                                "<td class=\"text-right\">" + downloadFile + deleteFile + "</td>" +                                                                        
                            "</tr>"
                        );
                    });
                }
            }
        });
    },
    delete: function(attachId) {
        
        $.ajax({
            type: "delete",
            url: "{{routeApi('SchdAttach/delete')}}/"  + attachId ,     
            crossDomain:true,           
            dataType: "json",
            success: function (response) {                    
                if (response.status) {
                    DocAttach.list( $('#frmHdrId').val());
                } else {
                    MessageNotify.error(response.message);
                }
            }
        });
    }
}

var DocForm = {
    init: function() {
        $.ajax({
            type: "get",
            url: "{{routeApi('Doc/')}}" + $('#docId').val(),     
            crossDomain:true,           
            dataType: "json",           
            success: function (response) {            
                var data = response.data;          
                if (data) {                         
                    var fbRender = document.getElementById("fb-render");                            
                    var formData = data.frmTemplate;
                    $(fbRender).formRender({ formData });
                }
            }
        });    
    },
    load: function() {
        $.ajax({
            type: "get",
            url: "{{routeApi('SchdFrm/getByActivityOwner')}}",     
            crossDomain:true,           
            dataType: "json",
            data : {
                    'activityId' : $('#activityId').val(),
                    'itsUserId' : $('#itsUserId').val(),
                    'personTypeId' : $('#personTypeId').val()                   
            },
            success: function (response) {   
                            
                if (response.status) {
                    var data = response.data;

                    if (data!=null) {                        
                        $('#frmHdrId').val(data.frmHdrId);
                        DocAttach.list(data.frmHdrId);
                    }
                    
                }
            }
        });
    }
}    
</script>
@endsection