@extends('layout.app')

@section('style')
@include('plugin.dropzone.css')
@endsection

@section("page-header")
<div class="page-header-title">
    <h5 class="m-b-10">{{ $docName }} ประจำภาค {{ $semCd }} / {{ $year }}</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('') }}"><i class="fa fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="{{ $_SESSION['URL'] }}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="javascript:void();">{{ $docName }}</a></li>
</ul>
@endsection

@section('content')

<form id="frmSave" name="frmSave" method="post" action="{{ routeApi ('SchdFrm/save') }}" enctype="multipart/form-data">
<div class="card shadow mb-5 rounded card-border-c-blue " id="div-doctype-form"> 
    <div class="card-body">                      
        <input type="hidden" name="frmHdrId" id="frmHdrId">
        <input type="hidden" name="roundId" id="roundId" value="{{ $roundId }}">
        <input type="hidden" name="activityId" id="activityId" value="{{ $activityId }}">
        <input type="hidden" name="docId" id="docId" value="{{ $docId }}">
        <input type="hidden" name="itsUserId" id="itsUserId" value="{{ $itsUserId }}">
        <input type="hidden" name="personTypeId" id="personTypeId" value="{{ $personTypeId }}">
        
        <!-- Form -->
        <div id="div-form"></div>

        <!-- File Upload -->
        <div id="div-upload" class="dropzone">
            <div class="dz-default dz-message">
                <span>ลากไฟล์มาตรงนี้ หรือ คลิกเลือกไฟล์ <br>
                    เฉพาะไฟล์ 
                    <i class="h3 fa fa-file-pdf text-danger" title="PDF"></i> 
                    <i class="h3 fa fa-file-word text-primary" title="WORD"></i>
                    <i class="h3 fa fa-file-excel text-success" title="EXCEL"></i>
                    <i class="h3 fa fa-file-powerpoint text-danger" title="POWERPONT"></i>
                    <i class="h3 fa fa-image text-info" title="IMAGE PNG,JPG"></i>
                </span> <br>และขนาดไฟล์ไม่เกิน 20MB เท่านั้น
            </div>
        </div>

        @include('document.uploadlist')
    </div>
    <div class="card-footer text-right">
        <button class="btn btn-sm btn-primary" type="submit"  id="btnSave"><i class="fa fa-save"></i> บันทึก</button>        
    </div>
</div>    
</form>


<div class="card shadow mb-5 rounded card-border-c-blue " id="div-check"> 
    <div class="card-body">
        <button class="btn btn-sm btn-info text-white d-none" type="button" id="btnSend"><i class="fas fa-paper-plane text-white"></i> ส่งงาน</button>
    </div>
</div>    

{{-- @include('document.status')      --}}
@endsection

@section('script')
@include('plugin.jqueryui.js')
@include('plugin.formbuilder.js')
@include('plugin.dropzone.js')
<script>
Dropzone.autoDiscover = false;  
var frmObj;
$(document).ready(function () {        
    frmObj = $('#frmSave').validate();
    
   
    $('#frmSave').submit(function (e) { 
        e.preventDefault();

        var frm = $(this);

        if (frmObj.valid()) {
            if (DocAttach.dropZone.files.length==0) {
                $.ajax({
                    type: "POST",
                    url: frm.attr('action'),
                    data: frm.serialize(),
                    dataType: "json",
                    success: function (response) {
                        if (response.status) {
                            $('#frmHdrId').val(response.data);
                            DocAttach.list( $('#frmHdrId').val());

                            MessageNotify.saveSucess();
                        }                        
                    }
                });
            } else {                
                DocAttach.dropZone.processQueue();
            }            
        }                                        
    });

    $('#btnSend').click(function(e) {
        DocStatus.send($('#frmHdrId').val());
    });
        
    DocAttach.init();
    DocForm.init();    
    DocStatus.init();
});

var DocAttach = {
    dropZone: null,
    init: function() {         
        $("#div-upload").dropzone({
            url: $('#frmSave').attr('action'),
            acceptedFiles: ".pdf,.doc,.docx,.xls,.xlsx,.ppt,.pptx,image/png,image/jpg,image/jpeg",
            maxFiles : 1,
            maxFilesize: 20,
            addRemoveLinks: true,
            autoProcessQueue: false,
            uploadMultiple: true,
            paramName: 'file',
            addRemoveLinks: true,
            init: function() {                
                this.on("addedfile", function (file) {
                    
                });

                this.on("removedfile", function (file) {
                    
                });

                this.on("maxfilesexceeded", function() {
                    if (this.files[1]!=null){                    
                        this.removeFile(this.files[1]);
                    }
                });                  
                
                this.on("success", function(file, responseText) {
                    this.removeAllFiles();                    
                    
                    if (responseText.status) {                                                
                        $('#frmHdrId').val(responseText.data);
                    }

                    DocAttach.list( $('#frmHdrId').val());

                    MessageNotify.saveSucess();
                });

                this.on("sending",function(data,xhr,formData) {                    
                    $.each($('#frmSave').serializeArray(), function (index, item) { 
                        formData.append(item.name, item.value);                             
                    });                                        
                });

                DocAttach.dropZone = this;                                    
            },               
            error: function (file, message, xhr) {
                if (xhr == null) this.removeFile(file); // perhaps not remove on xhr errors
                // alert(message);  
                MessageNotify.error(message);
            },
            // success: function (file, response) {
            //     var imgName = response;
            //     file.previewElement.classList.add("dz-success");
            //     console.log("Successfully uploaded :" + imgName);
            // },
            // error: function (file, response) {
            //     file.previewElement.classList.add("dz-error");
            // }
        });        
    },
    list : function(refDocId) {        
        $('#tblAttachList tbody').empty();
        $.ajax({
            type: "get",
            url: "{{routeApi('SchdAttach/getByRefDocId')}}/"  + refDocId ,     
            crossDomain:true,           
            dataType: "json",
            success: function (response) {                    
                if (response.data) {
                    var data = response.data;
                    
                    $.each(data, function (idx, item) {            
                        var urlDownload = "{{routeApi('SchdAttach/download')}}/"  + item.attachId;
                        var urlDelete = "javascript:DocAttach.delete(" + item.attachId + ");";
                        var downloadFile = "<a class=\"btn btn-sm btn-info\" href=\"" + urlDownload + "\"><i class=\"fa fa-folder-open\"></i>เปิดไฟล์</a>";
                        var deleteFile = "<a class=\"btn btn-sm btn-danger\" href=\"" + urlDelete + "\"><i class=\"fa fa fa-trash\"></i>ลบไฟล์</a>";
                        $('#tblAttachList tbody').append(
                            "<tr>" +
                                "<td class=\"text-left\">" + item.fileName + "</td>" +
                                "<td class=\"text-right\">" + downloadFile + deleteFile + "</td>" +                                                                        
                            "</tr>"
                        );
                    });
                }
            }
        });
    },
    delete: function(attachId) {
        
        $.ajax({
            type: "delete",
            url: "{{routeApi('SchdAttach/delete')}}/"  + attachId ,     
            crossDomain:true,           
            dataType: "json",
            success: function (response) {                    
                if (response.status) {
                    DocAttach.list( $('#frmHdrId').val());
                } else {
                    MessageNotify.error(response.message);
                }
            }
        });
    }
}

var DocForm = {
    init: function() {
        $.ajax({
            type: "get",
            url: "{{routeApi('Doc/')}}" + $('#docId').val(),     
            crossDomain:true,           
            dataType: "json",           
            success: function (response) {            
                var data = response.data;          
                if (data) {
                    var formData = data.frmTemplate;
                    var form = $('#div-form');      
                                       
                    if (formData!=null) {
                        var formRenderOpts = {
                            formData,
                            notify: {
                                error: function(message) {
                                    MessageNotify.error(message);
                                },
                                success: function(message) {
                                    $('#frmSave :input:enabled:visible:first').focus();

                                    DocForm.load(formData);
                                },
                                warning: function(message) {
                                    MessageNotify.error(message);
                                }
                            }
                        };

                        form.formRender(formRenderOpts);
                    } else {
                        DocForm.load(formData);
                    }
                               
                }
            }
        });    
    },
    defaultValue: function(template) {        
        var json = JSON.parse(template);
        var studentType = "{{ getenv('PERSON_TYPE_STUDENT') }}";
        var currentPersonTypeId = "{{ $_SESSION['PERSON_TYPE_ID'] }}";
        var currentName = "{{ $_SESSION['NAME'] }}";
        $.each(json, function (index, item) { 
            if (item.name) {
                if (item.name=="sem") {
                    $('#sem').val("{{ $semCd }}");
                    $('#sem').attr('readOnly',true);
                } else if (item.name=="year") {
                    $('#year').val("{{ $year }}");
                    $('#year').attr('readOnly',true);
                } else if (item.name=="std_fullname") {
                    if (studentType==currentPersonTypeId) {
                        $('#std_fullname').val(currentName);
                        $('#std_fullname').attr('readOnly',true);
                    }
                }
            }
        });  
                
        $('.readOnly').each(function (index, element) {
            $(this).attr('readOnly',true);            
        });
    },
    load: function(template) {
        $.ajax({
            type: "get",
            url: "{{routeApi('SchdFrm/getByActivityOwner')}}",     
            crossDomain:true,           
            dataType: "json",
            data : {
                    'activityId' : $('#activityId').val(),
                    'itsUserId' : $('#itsUserId').val(),
                    'personTypeId' : $('#personTypeId').val()                   
            },
            success: function (response) {
                if (response.status) {
                    var data = response.data;

                    if (data.hdr) {                        
                        $('#frmHdrId').val(data.hdr.frmHdrId);

                        $('#btnSend').removeClass('d-none');

                        DocAttach.list(data.hdr.frmHdrId);
                    }
                    
                    if (data.dtl) {
                        $.each(data.dtl, function (index, item) { 
                            $('#' + item.question).val(item.answer);
                        });
                    }

                    if (template!=null) {
                        DocForm.defaultValue(template);                    
                    }                    
                }
            }
        });
    }
}    

var DocStatus = {
    init: function() {
        var iconPath = "{{ routeApi('Profile/getImageIcon/') }}";    
            
            var itsStudentId = $('#itsStudentId').val();

            $('#person-relate').empty();
            $.ajax({
                type: "get",
                url: "{{routeApi('SchdStdInfo/getRelateByItsStudent/')}}" + itsStudentId,     
                crossDomain:true,           
                dataType: "json",
                success: function (response) {                                        
                    if (response.data) {
                        var data = response.data;       
                        
                        $.each(data, function (idx, item) {       
                            
                            $('#person-relate').append(
                                "<div class=\"align-middle m-b-25\">" +     
                                    "<img src=\"" +   iconPath + item.itsId  +   "\" alt=\"user image\" class=\"img-radius align-top m-r-15\" width=\"80px\">" + 
                                    "<div class=\"d-inline-block\">" +
                                        "<h6>" + item.itsName + "</h6>" +                                         
                                        "<p class=\"m-b-0\">" + item.personTypeNameTh +"</p>" +                                        
                                    "</div>" +
                                "</div>"
                            );                            
                            
                        });                        
                    }
                }
            });
        },


        $('#doctype-activity-status-form').on('shown.bs.modal', function() {
            $('#doctype-activity-status-remark').focus();
        });

        $('#frmDocStatus').submit(function (e) {
            e.preventDefault();

            if ($(this).valid()) {            
                $.ajax({
                    type: 'post',
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function (response) {                    
                        DocTypeCourse.clearScreen();
                        
                        if (response.status) {
                            if (response.data) {                                
                                
                                $('#doctype-activity-status-form').modal('hide');
                                DocTypeCourse.loadData();
                            }
                        } else {
                            MessageNotify.error(response.message);
                        }                        
                    }
                });
            }
        });
    },
    send : function (refDocId) {        
        
        $('#frmDocStatus input[name=refDocId]').val(refDocId);
        $('#frmDocStatus input[name=activityId]').val($('#activityId').val());
        $('#frmDocStatus input[name=docId]').val($('#docId').val());
        $('#frmDocStatus input[name=itsUserId]').val($('#itsStudentId').val());
        $('#frmDocStatus input[name=personTypeId]').val($('#personTypeId').val());
        $('#doctype-activity-status-form').modal('show');
    }
}
</script>
@endsection