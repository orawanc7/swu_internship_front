
<div class="card shadow mb-5 rounded card-border-c-blue " id="div-doctype-course-list"> 
    <div class="card-body">         
        <input type="hidden" id="activityId" name="activityId" value="{{ $activityId }}">
        <input type="hidden" id="docId" name="docId" value="{{ $docId }}">
        <input type="hidden" id="itsStudentId" name="itsStudentId" value="{{ $itsUserId }}">
        <input type="hidden" id="personTypeId" name="personTypeId" value="{{ $itsUserPersonTypeId }}">
        <input type="hidden" id="stdCourseId" name="stdCourseId">    
        <div class="form-group row">            
            <label for="itsCourseCd" class="col-md-2 col-form-label form-control-label mandatory">รหัสวิชา</label>
            <div class="col-md-4">
                <div class="input-data">
                    <input type="text" name="itsCourseCd" class="form-control" placeholder="รหัสวิชาที่" required maxlength="15" autofocus>
                </div>
            </div>
            <label for="itsCourseName" class="col-md-2 col-form-label form-control-label mandatory">ชื่อรายวิชา</label>
            <div class="col-md-4">
                <div class="input-data">
                    <input type="text" name="itsCourseName" class="form-control" placeholder="ชื่อรายวิชา" required maxlength="100">
                </div>
            </div>                        
        </div>            
        <div class="form-group row">
            <label for="levelClass" class="col-md-2 col-form-label form-control-label mandatory">ระดับชั้น</label>
            <div class="col-md-4">
                <div class="input-data">
                    <input type="text" name="levelClass" class="form-control" placeholder="ระดับชั้น" required maxlength="100">
                </div>
            </div>
            <label for="creditAmt" class="col-md-2 col-offset-sm-2 col-form-label form-control-label mandatory">หน่วยกิต</label>
            <div class="col-md-4">
                <div class="input-data">
                    <input type="text" name="creditAmt" class="form-control text-right" required>
                </div>
            </div>                               
        </div>
        <div class="form-group row">            
            <label for="minutesQty" class="col-md-2 col-form-label form-control-label mandatory">นาทีต่อ 1 คาบ</label>
            <div class="col-md-4">
                <div class="input-data">
                    <input type="text" name="minutesQty" class="form-control text-right" required data-error="">
                </div>
            </div>     
            <label for="hourAmt" class="col-md-2 col-form-label form-control-label mandatory">ชั่วโมงทั้งวิชา</label>
            <div class="col-md-4">                
                <div class="input-data">
                    <input type="text" name="hourAmt" class="form-control text-right" required maxlength="30">                
                </div>
            </div>                           
        </div>
        <div class="form-group row">            
            <label for="remark" class="col-md-2 col-form-label form-control-label">หมายเหตุ</label>
            <div class="col-md-10">                
                <input type="text" name="remark" class="form-control" maxlength="200">                
            </div>                           
        </div>       
        
        @include('document.uploadlist1')
    </div>
    <div class="card-footer text-right">
        <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i> บันทึก</button>
        <button class="btn btn-sm btn-secondary" type="reset" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>
    </div>
</div>