<div class="card shadow mb-5 rounded card-border-c-blue " id="div-doctype-course-list"> 
    <div class="card-header">
        <i class="fas fa-list"> รายวิชาทั้งหมด</i>
        <div class="card-header-right">
            <div class="btn-group card-option">
                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="feather icon-more-horizontal"></i>
                </button>
                <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                    <li class="dropdown-item full-card"><a href="javascript:void();"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                    <li class="dropdown-item minimize-card"><a href="javascript:void();"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>                    
                </ul>
            </div>
        </div>
    </div>   
    <div class="card-body">                                            
        <div class="table-responsive">
            <table class="table table-hover m-0" id="tbDocTypeCourse">                           
                <thead>
                        <tr class="bg-primary">                
                            <th style="width:10%">รหัสวิชา</th>
                            <th style="width:24%">ชื่อรายวิชา</th>
                            <th style="width:14%">ระดับชั้น</th>
                            <th style="width:12%" class="text-left text-sm-right">หน่วยกิต</th>
                            <th style="width:12%" class="text-left text-sm-right">นาทีต่อ 1 คาบ</th>
                            <th style="width:12%" class="text-left text-sm-right">ชั่วโมงทั้งวิชา</th>                
                            <th style="width:16%" class="text-left text-sm-right"></th>                                                              
                        </tr>
                </thead>            
                <tbody>
                </tbody>
            </table>     
        </div>           
    </div>      
</div>   