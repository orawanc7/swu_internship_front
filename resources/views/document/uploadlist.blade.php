<div class="table-responsive">
    <table class="table table-hover mt-1" id="tblAttachList">
        <thead>
            <tr>                                                                               
                <th class="text-left" style="width:70%"><i class="fa fa-folder-open" title="PDF"></i> ไฟล์แนบ</th>                        
                <th class="text-center" style="width:30%"></th>                        
            </tr>
        </thead>
        <tbody>                    
        </tbody>                
    </table>                
</div>