<div class="row" id="div-doctype-upload">
    <div class="col-md-2">                
        <h6 class="m-b-15">เอกสารแนบ</h6>
    </div>
    <div class="col-md-10">
        <button class="btn btn-sm btn-primary" id="btnFileUpload" type="button"><i class="fa fa-upload"></i> แนบไฟล์</button>
        <div id="div-doctype-upload-list" class="mt-2">
        </div>
    </div>            
</div>