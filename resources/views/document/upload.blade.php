<div class="modal fade" id="upload" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">            
            <div class="modal-header">
				<h5 class="modal-title"><i class="fa fa-upload"></i> เอกสารแนบ/ลิงค์แนบ</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
			</div>

            <div class="modal-body">            
                <div class="form-group row">
                    <div class="col-md-12">            
                        <div class="form-group">
                            <div class="radio radio-primary d-inline">
                                <input type="radio" name="uploadType" id="rdoAttachTypeF" checked="" value="F">
                                <label for="rdoAttachTypeF" class="cr">แนบไฟล์</label>
                            </div>
                        </div>
                                                                        
                        <form id="upload-form" method="post" action="" class="dropzone dz-clickable">
                            <div class="dz-default dz-message">
                                <span>ลากไฟล์มาตรงนี้ หรือ คลิกเลือกไฟล์ <br>
                                    เฉพาะไฟล์ 
                                    <i class="h3 icofont icofont-file-pdf text-danger" data-toggle="tooltip" data-placement="top" title="PDF"></i> 
                                    <i class="h3 icofont icofont-file-word text-blue" data-toggle="tooltip" data-placement="top" title="WORD"></i>
                                    <i class="h3 icofont icofont-file-excel text-success" data-toggle="tooltip" data-placement="top" title="EXCEL"></i>
                                    <i class="h3 icofont icofont-file-powerpoint text-danger" data-toggle="tooltip" data-placement="top" title="POWERPONT"></i>
                                    <i class="h3 icofont icofont-file-png text-primary" data-toggle="tooltip" data-placement="top" title="IMAGE PNG"></i>
                                    <i class="h3 icofont icofont-file-jpg text-primary" data-toggle="tooltip" data-placement="top" title="IMAGE JPG"></i>                        
                                </span> <br>และขนาดไฟล์ไม่เกิน 20MB เท่านั้น
                            </div>                             
                        </form>                
                    </div>
                </div>   
                {{-- <div class="form-group row">
                    <div class="col-md-12">                                                
                        <div class="form-group">
                            <div class="radio radio-primary d-inline">
                                <input type="radio" name="uploadType" id="rdoAttachTypeL" value="L">
                                <label for="rdoAttachTypeL" class="cr">แนบลิงค์</label>
                            </div>
                        </div>                        
                    </div>
                </div>            --}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary " data-dismiss="modal"><i class="fa fa-times"></i> ปิด</button>
                <button type="button" id="btnUpload" class="btn btn-sm  btn-primary d-none"><i class="fa fa-upload"></i> แนบไฟล์</button>                
                <button type="button" id="btnSaveLink" class="btn btn-sm btn-primary d-none"><i class="fas fa-link"></i> แนบลิงค์</button>                
            </div>
        </div>
    </div>
</div>