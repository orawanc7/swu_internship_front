@extends('layout.app')

@section("page-header")
@endsection

@section('content')

<input type="hidden" name="itsPersonId" id="itsPersonId" value="{{ $_SESSION['ITS_ID'] }}">
<input type="hidden" name="activitySetNo" id="activitySetNo">
<input type="hidden" name="personTypeId" id="personTypeId" value="{{ $_SESSION['PERSON_TYPE_ID'] }}">

<div class="row">        
    <div class="col-xl-8">        
        <div class="row">
            <div class="col-xl-12">
                <div class="input-group mb-3">
                    <div class="input-group-append">
                        <span class="input-group-text">ภาค/ปีการศึกษา</span>
                      </div>
                      <select name="roundId" id="roundId" class="form-control"></select>                    
                </div>
            </div>            
        </div>
                

        <div class="row">
            <div class="col-xl-12">
                @include("teacher.seminar")
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12">
                @include("teacher.doccheck")
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12">
                @include("teacher.docowner")
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12">
                @include("teacher.teacherstudentlist")
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12">
                @include("teacher.mentorstudentlist")
            </div>
        </div>
        
    </div>
    <div class="col-xl-4">
        <div class="row">
            <div class="col-xl-12">
                @include("share.download")
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12">
                @include("share.news")
            </div>
        </div>
    </div>
</div>    
                        
@endsection

@section('script')
@include('plugin.datetime.js')
<script src="@relative('assets/pages/docdownload.js')?time()"></script>
<script>    
    $(document).ready(function () {
       
        $('#roundId').change(function (e) { 
            var roundId = $(this).val();
            Teacher.loadSeminar(roundId);    
            Teacher.loadTeacherStudent(roundId);
            Teacher.loadMentorStudent(roundId);
            Teacher.loadDocCheck(roundId);
            Teacher.loadDocOwner(roundId);
        });

        $("#tblSeminar tbody").on("click", "button.semimar-check-in", function(e) {
            var activityId = $(this).attr('data-activity-id');
            Teacher.seminarCheck('IN',activityId);
        });

        $("#tblSeminar tbody").on("click", "button.semimar-check-out", function(e) {
            var activityId = $(this).attr('data-activity-id');
            Teacher.seminarCheck('OUT',activityId);
        });

        $('#tblDocCheck,tblDocOwner').on("click", "button.doc-activity", function () {
            
            var activityId = $(this).attr('data-activity-id');            
            
            location.href = "{{ route('document') }}/" + activityId + '/' + $('#itsPersonId').val();
        });

        var pxts = new PerfectScrollbar('.teacher-studentlist-scroll', {
            wheelSpeed: .5,
            swipeEasing: 0,
            wheelPropagation: 1,
            minScrollbarLength: 40,
        });

        var pxms = new PerfectScrollbar('.mentor-studentlist-scroll', {
            wheelSpeed: .5,
            swipeEasing: 0,
            wheelPropagation: 1,
            minScrollbarLength: 40,
        });

        var pxdc = new PerfectScrollbar('.doccheck-scroll', {
            wheelSpeed: .5,
            swipeEasing: 0,
            wheelPropagation: 1,
            minScrollbarLength: 40,
        });

        var pxdo = new PerfectScrollbar('.docowner-scroll', {
            wheelSpeed: .5,
            swipeEasing: 0,
            wheelPropagation: 1,
            minScrollbarLength: 40,
        });
        
        Teacher.loadRound();        
    });

    var Teacher = {
        loadRound: function() {
            $.ajax({
                type: "get",
                url: "{{routeApi('SchdRound/getAll')}}",     
                crossDomain:true,           
                dataType: "json",
                success: function (response) {                    
                    if (response.data) {
                        var data = response.data;
                        
                        $.each(data, function (idx, item) {
                            $('#roundId').append($('<option>', { 
                                value: item.roundId,
                                text : item.roundNameTh 
                            }));
                        });              
                        $('#roundId').trigger('change');
                    }
                }
            });
        },        
        
        loadSeminar: function(roundId) {
            var itsPersonId = $('#itsPersonId').val();
            var personTypeId = $('#personTypeId').val();

            $('#tblSeminar tbody').empty();
            $.ajax({
                type: "get",
                url: "{{routeApi('SchdSeminar/getSeminarByRoundAttendee')}}",     
                crossDomain:true,           
                dataType: "json",
                data : {
                    'roundId' : roundId,
                    'personTypeId' : personTypeId,
                    'attendeeId' : itsPersonId
                },
                success: function (response) {                    
                    if (response.data) {
                        var data = response.data;
                        
                        $.each(data, function (idx, item) {
                            var start = "";
                            var end = "";
                            var rangeDate = "";
                            var startDate = "";
                            var endDate = "";
                            var seminarStartDate = "";
                            var seminarEndDate = "";

                            if (item.startDate!=null) {
                                startDate = moment(item.startDate.date).format("DD/MM/") + (parseInt(moment(item.startDate.date).format("YYYY"))+543);                                
                            }
                            if (item.endDate!=null) {
                                endDate = moment(item.endDate.date).format("DD/MM/") + (parseInt(moment(item.endDate.date).format("YYYY")) + 543);                                
                            }

                            if (startDate==endDate) {
                                rangeDate = startDate;
                            } else {
                                rangeDate = startDate + "-" + endDate;
                            }

                            
                            if (item.seminarStartDate!=null) {
                                seminarStartDate = moment(item.seminarStartDate.date).format("DD/MM/") +
                                                            (parseInt(moment(item.seminarStartDate.date).format("YYYY"))+543) + " " +
                                                            moment(item.seminarStartDate.date).format("HH:mm");                                                            
                                start = seminarStartDate;
                            } else {
                                start = "<button class=\"btn btn-sm btn-primary semimar-check-in\" data-activity-id=\"" + item.activityId + "\"><i class=\"feather icon-map-pin\"></i> Check In</button>";
                            }

                            if (item.seminarEndDate!=null) {                                
                                seminarEndDate = moment(item.seminarEndDate.date).format("DD/MM/") +
                                                            (parseInt(moment(item.seminarEndDate.date).format("YYYY"))+543) + " " +
                                                            moment(item.seminarEndDate.date).format("HH:mm");                             
                                end = seminarEndDate;
                            } else {                  
                                if (item.seminarStartDate!=null) {              
                                    end = "<button class=\"btn btn-sm btn-success semimar-check-out\" data-activity-id=\""+ item.activityId + "\"><i class=\"feather icon-log-out\"></i> Check Out</button>";
                                } else {
                                    end = "<button class=\"btn btn-sm btn-success semimar-check-out disabled\" data-activity-id=\""+ item.activityId + "\"><i class=\"feather icon-log-out\"></i> Check Out</button>";
                                }
                            }
                           

                            $('#tblSeminar tbody').append(
                                "<tr>" +
                                    "<td class=\"text-center\">" + start + "</td>" +
                                    "<td class=\"text-center\">" + end + "</td>" +
                                    "<td>" + item.activityNameTh + "</td>" +
                                    "<td>" + rangeDate + "</td>" +                                                                        
                                "</tr>"
                            );
                        });
                    }
                }
            });
        },       
        seminarCheck: function(type,activityId) {
            var roundId = $('#roundId').val();
            $.ajax({
                type: "post",
                url: "{{routeApi('SchdSeminar/saveCheck')}}" ,     
                data : {
                    'activityId' : activityId,
                    'personTypeId' : $('#personTypeId').val(),
                    'attendeeId' : $('#itsPersonId').val(),
                    'checkType' : type
                },
                crossDomain:true,           
                dataType: "json",
                success: function (response) {                    
                    if (response) {
                        Teacher.loadSeminar(roundId);
                    }
                }
            });
        },   
        loadTeacherStudent: function() {            
            var itsPersonId = $('#itsPersonId').val();
            var roundId = $('#roundId').val();

            $('#tblTeacherStudent tbody').empty();
            $.ajax({
                type: "get",
                url: "{{routeApi('SchdPersonStd/getStudentByPersonRound')}}",     
                crossDomain:true,           
                dataType: "json",
                data : {
                    'roundId' : roundId,
                    'itsPersonId' : itsPersonId
                },
                success: function (response) {                                        
                    if (response.data) {
                        var data = response.data;
                        
                        var count = 0;
                        var iconPath = "{{ routeApi('Profile/getImageIcon/') }}";
                        var studentPath = "{{ route('teacher/student/') }}";
                        $.each(data, function (idx, item) {                            
                            $('#tblTeacherStudent tbody').append(
                                "<tr>" +
                                    "<td class=\"text-center align-middle\"><img src=\"" + iconPath + item.studentId + "\" class=\"img-radius\" width=\"40px\"/></td>" +
                                    "<td class=\"text-center align-middle\"><a href=\"" + studentPath + item.itsStudentId + "\">" + item.studentId + "</a></td>" +
                                    "<td class=\"align-middle\">" + item.prenameSnameTh + item.fnameTh + " " + item.lnameTh + "</td>" +
                                    "<td class=\"text-center align-middle\">" + item.deptSnameTh + "-" + item.majorSnameTh + "</td>" +
                                    "<td class=\"text-center align-middle\">" + item.courseCd + "</td>" +                                                                        
                                    "<td class=\"text-center align-middle\">" + item.schoolNameTh + "</td>" +                                                                        
                                "</tr>"                                
                            );
                            count++;
                        });

                        if (count>0) {
                            $('#div-teacher-student-list').removeClass('d-none');
                        }

                        $('#spnTeacherStudentCount').html(count);
                    }
                }
            });
        },
        loadMentorStudent: function() {            
            var itsPersonId = $('#itsPersonId').val();
            var roundId = $('#roundId').val();

            $('#tblMentorStudent tbody').empty();
            $.ajax({
                type: "get",
                url: "{{routeApi('SchdMentorStd/getStudentByPersonRound')}}",     
                crossDomain:true,           
                dataType: "json",
                data : {
                    'roundId' : roundId,
                    'itsPersonId' : itsPersonId
                },
                success: function (response) {                                        
                    if (response.data) {
                        var data = response.data;
                        
                        var count = 0;
                        var iconPath = "{{ routeApi('Profile/getImageIcon/') }}";
                        var studentPath = "{{ route('teacher/student/') }}";
                        $.each(data, function (idx, item) {                            
                            $('#tblMentorStudent tbody').append(
                                "<tr>" +
                                    "<td class=\"text-center align-middle\"><img src=\"" + iconPath + item.studentId + "\" class=\"img-radius\" width=\"40px\"/></td>" +
                                    "<td class=\"text-center align-middle\"><a href=\"" + studentPath + item.itsStudentId + "\">" + item.studentId + "</a></td>" +
                                    "<td class=\"align-middle\">" + item.prenameSnameTh + item.fnameTh + " " + item.lnameTh + "</td>" +
                                    "<td class=\"text-center align-middle\">" + item.deptSnameTh + "-" + item.majorSnameTh + "</td>" +
                                    "<td class=\"text-center align-middle\">" + item.courseCd + "</td>" +                                                                        
                                    "<td class=\"text-center align-middle\">" + item.schoolNameTh + "</td>" +                                                                        
                                "</tr>"                                
                            );
                            count++;
                        });

                        if (count>0) {
                            $('#div-mentor-student-list').removeClass('d-none');
                        }

                        $('#spnMentorStudentCount').html(count);
                    }
                }
            });
        },
        loadDocCheck(roundId) {
            $('#tblDocCheck tbody').empty();
            $.ajax({
                type: "get",
                url: "{{routeApi('SchdDocStatus/getByRoundChecker')}}",     
                crossDomain:true,           
                dataType: "json",
                data : {
                    'roundId' : roundId,
                    'personTypeId' : $('#personTypeId').val(),
                    'itsUserId' : $('#itsPersonId').val()
                },
                success: function (response) {       
                    
                    if (response.status) {
                        var data = response.data;                  
                        var statusWait = "";
                        var statusReturn = "";
                        var statusSuccess = "";
                        var startDate = "";
                        var endDate = "";
                        var docDate = "";
                        $.each(data, function (idx, item) {     

                            if (item.startDate!=null) {
                                startDate = moment(item.startDate.date).format("DD/MM/") + (parseInt(moment(item.startDate.date).format("YYYY"))+543);                                
                            }
                            if (item.endDate!=null) {
                                endDate = moment(item.endDate.date).format("DD/MM/") + (parseInt(moment(item.endDate.date).format("YYYY")) + 543);                                
                            }

                            if (startDate==endDate) {
                                docDate = startDate;
                            } else {
                                docDate = startDate + "-" + endDate;
                            }

                            statusWait = "<span class=\"badge badge-primary\" style=\"font-size:100%\">" + item.waitCount + "</span>&nbsp;";
                            statusReturn = "<span class=\"badge badge-warning\" style=\"font-size:100%\">" + item.returnCount + "</span>&nbsp;";                            
                            statusSuccess = "<span class=\"badge badge-success\" style=\"font-size:100%\">" + item.passCount + "</span>&nbsp;";

                            var action = "";
                            if ((item.waitCount + item.returnCount  + item.passCount) >0) {
                                action = "<button class=\"btn btn-sm btn-primary doc-activity\" data-activity-id=\"" + item.activityId + "\"><i class=\"fas fa-check-double\"></i> รายละเอียด</button>";
                            }
                            
                            $('#tblDocCheck tbody').append(
                                "<tr>" +                                    
                                    "<td class=\"text-center align-middle\">" + statusWait + statusReturn +  statusSuccess + "</td>" +                                    
                                    "<td class=\"text-center align-middle\">" + action + "</td>" +
                                    "<td class=\"align-middle\">" + item.docNameTh + "</td>" +
                                    "<td class=\"text-center align-middle\">" + docDate + "</td>" +                                                                                                            
                                "</tr>"                                
                            );                            
                        });                           
                    }
                }
            });
        },    
        loadDocOwner(roundId) {
            $('#tblDocOwner tbody').empty();
            $.ajax({
                type: "get",
                url: "{{routeApi('SchdDocStatus/getByRoundActivitySetNoOwner')}}",     
                crossDomain:true,           
                dataType: "json",
                data : {
                    'roundId' : roundId,
                    'activitySetNo' : $('#activitySetNo').val(),
                    'personTypeId' : $('#personTypeId').val(),
                    'itsUserId' : $('#itsPersonId').val()
                },                
                success: function (response) {       
                    
                    if (response.status) {
                        var data = response.data;                  
                        var status = "";
                        var statusDate = "";
                        var startDate = "";
                        var endDate = "";
                        var docDate = "";
                        $.each(data, function (idx, item) {     

                            if (item.startDate!=null) {
                                startDate = moment(item.startDate.date).format("DD/MM/") + (parseInt(moment(item.startDate.date).format("YYYY"))+543);                                
                            }
                            if (item.endDate!=null) {
                                endDate = moment(item.endDate.date).format("DD/MM/") + (parseInt(moment(item.endDate.date).format("YYYY")) + 543);                                
                            }

                            if (startDate==endDate) {
                                docDate = startDate;
                            } else {
                                docDate = startDate + "-" + endDate;
                            }

                            if (item.statusFlag==null) {
                                status = "<button class=\"btn btn-sm btn-primary\"><i class=\"fas fa-pen-alt\"></i> บันทึก</button>";
                            } else if (item.statusFlag == "Y") {
                                status = "<span class=\"badge badge-success\">ผ่าน</span>";
                                passCount++;
                            } else if (item.statusFlag == "R") {
                                status = "<span class=\"badge badge-warning\">ส่งกลับ</span>";
                                returnCount++;
                            } else if (item.statusFlag == "W") {
                                status = "<span class=\"badge badge-wait\">รอตรวจ</span>";
                                waitCount++;
                            }

                            $('#tblDocOwner tbody').append(
                                "<tr>" +                                    
                                    "<td class=\"text-center align-middle\">" + status + "</td>" +
                                    "<td class=\"text-center align-middle\">" + statusDate + "</td>" +
                                    "<td class=\"align-middle\">" + item.docNameTh + "</td>" +
                                    "<td class=\"text-center align-middle\">" + docDate + "</td>" +                                                                                                            
                                "</tr>"                                
                            );                            
                        });                           
                    }
                }
            });
        },        
        downloadDocStudent: function() {
            var itsPersonId = $('#itsPersonId').val();
            var roundId = $('#roundId').val();

            var url = "{{ routeApi ('SchdPersonStd/downloadDocStudent')}}" + "?itsPersonId=" + itsPersonId + "&roundId=" + roundId;
            
            location.href = url;
        }     
    }
</script>
@endsection
