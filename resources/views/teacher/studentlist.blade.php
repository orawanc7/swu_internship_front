<div class="card shadow mb-5 bg-white rounded card-border-c-blue">
    <div class="card-header">
        <h5><i class="feather icon-users"></i> นิสิตในความดูแลสำหรับ<u>อาจารย์นิเทศก์</u> จำนวน <span id="spnStudentCount" class="badge badge-primary"></span> คน</h5>
        <div class="card-header-right">
            <div class="btn-group card-option">
                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="feather icon-more-horizontal"></i>
                </button>
                <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                    <li class="dropdown-item full-card"><a href="javascript:void();"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                    <li class="dropdown-item minimize-card"><a href="javascript:void();"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>                    
                </ul>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="studentlist-scroll" style="height:465px;position:relative;">
            <div class="table-responsive">            
                <table class="table table-xl" id="tblStudent">
                    <thead>
                        <tr>
                            <th class="text-center" style="width:10%"></th>
                            <th class="text-center" style="width:10%">เลขประจำตัว</th>
                            <th class="text-left" style="width:30%">ชื่อ-สกุล</th>
                            <th class="text-left" style="width:20%">คณะ-สาขา</th>
                            <th class="text-center" style="width:10%">วิชา</th>                        
                            <th class="text-center" style="width:20%">สถานที่ฝึกประสบการณ์</th>                        
                        </tr>
                    </thead>
                    <tbody>                    
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="6" class="text-left"><a href="javascript:Teacher.downloadDocStudent();"><i class="feather icon-download"></i> ดาวน์โหลดรายชื่อ</a></td>                        
                        </tr>
                    </tfoot>
                </table>            
            </div>
        </div>
    </div>
</div>