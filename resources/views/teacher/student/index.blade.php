@extends('layout.app')

@section("style")

@endsection

@section("page-header")
<div class="page-header-title">
    <h5 class="m-b-10">รายละเอียดนิสิต</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('') }}"><i class="fa fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="{{ $_SESSION['URL'] }}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="javascript:void();">รายละเอียดนิสิต <span id="roundSemYear"></span></a></li>
</ul>
@endsection

@section('content')
<input type="hidden" name="itsId" id="itsId" value="{{ $_SESSION['ITS_ID'] }}">
<input type="hidden" name="itsStudentId" id="itsStudentId" value="{{ $itsStudentId }}">
<input type="hidden" name="activitySetNo" id="activitySetNo">
<input type="hidden" name="roundId" id="roundId" value="">
<input type="hidden" name="personTypeId" id="personTypeId" value="">

<div class="row">
    <div class="col-lg-4">                        
        @include('teacher.student.profile')        
    </div>

    <div class="col-lg-8">                
        @include('teacher.student.doc')       
        
        @include("teacher.student.docowner")
    </div>
</div>

<div class="row">
    <div class="col-12 d-none" id="div-student-other">
        @include('teacher.student.other')
    </div>
</div>

@endsection


@section('script')
<script>    
    $(document).ready(function () {
        var iconPath = "{{ routeApi('Profile/getImageIcon/') }}";    
        var studentPath = "{{ route('teacher/student/') }}";


        var px = new PerfectScrollbar('.doc-scroll', {
            wheelSpeed: .5,
            swipeEasing: 0,
            wheelPropagation: 1,
            minScrollbarLength: 40,
        });


        TeacherStudent.getInfo(iconPath,studentPath);                
    });

    var TeacherStudent = {        
        getInfo: function(iconPath,studentPath) {
            $.ajax({
                type: "get",
                url: "{{routeApi('SchdStdInfo/get/')}}" + $('#itsStudentId').val(),                
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        var data = response.data;

                        $('#roundId').val(data.roundId);
                        $('#activitySetNo').val(data.activitySetNo);
                        $('#personTypeId').val(data.personTypeId);

                        $('#roundSemYear').html("ประจำภาค"+data.semCd + "/" + data.year);
                        $('#studentImage').attr('src', iconPath + data.studentId);
                        
                        $('#studentName').html(data.prenameSnameTh + data.fnameTh + "<br>" + data.lnameTh);
                        $('#studentId').html(data.studentId);
                        $('#studentDept').html(data.deptLnameTh);
                        $('#studentMajor').html(data.majorLnameTh);
                        $('#studentCourse').html(data.courseCd);
                        $('#studentSchool').html(data.schoolNameTh);                        

                        $('#studentBuasriId').html(data.buasrId);   
                        $('#studentGafeEmail').html(data.gafeEmail);   
                        $('#studentOtherEmail').html(data.otherEmail);   
                        $('#studentMobileNo').html(data.mobileNo);   
                        $('#studentTelephoneNo').html(data.telephoneNo);   

                        TeacherStudent.loadDoc();
                        TeacherStudent.loadOther(iconPath,studentPath);                        
                    }
                }
            });
        },

        loadOther(iconPath,studentPath) {
            $.ajax({
                type: "get",
                url: "{{routeApi('SchdPersonStd/getStudentByPersonRound')}}",     
                crossDomain:true,           
                dataType: "json",
                data : {
                    'roundId' : $('#roundId').val(),
                    'itsPersonId' : $('#itsId').val()
                },
                success: function (response) {                                        
                    if (response.data) {
                        var data = response.data;       
                        if (data.length>1) {
                            $('#div-student-other').removeClass('d-none');
                        }
                        
                        $.each(data, function (idx, item) {       
                            if (item.itsStudentId!=$('#itsStudentId').val()) {
                                $('#student-other').append(
                                    "<div class=\"align-middle m-b-25\">" +     
                                        "<img src=\"" +   iconPath + item.studentId  +   "\" alt=\"user image\" class=\"img-radius align-top m-r-15\" width=\"40px\">" + 
                                        "<div class=\"d-inline-block\">" +
                                            "<a href=\"" + studentPath + item.itsStudentId +   "\">" +
                                                "<h6>" + item.prenameSnameTh + item.fnameTh + " " + item.lnameTh + "</h6>" + 
                                            "</a>" + 
                                            "<p class=\"m-b-0\"><i class=\"fas fa-landmark\"></i> " + item.schoolNameTh +"</p>" +                                        
                                        "</div>" +
                                    "</div>"
                                );                            
                            }
                        });                        
                    }
                }
            });
        },
        
        loadDoc() {
            $('#tblDoc tbody').empty();
            $.ajax({
                type: "get",
                url: "{{routeApi('SchdDocStatus/getByRoundActivitySetNoOwner')}}",     
                crossDomain:true,           
                dataType: "json",
                data : {
                    'roundId' : $('#roundId').val(),
                    'activitySetNo' : $('#activitySetNo').val(),
                    'personTypeId' : $('#personTypeId').val(),
                    'itsUserId' : $('#itsStudentId').val()
                },
                success: function (response) {       
                    
                    if (response.status) {
                        var data = response.data;                  
                        var waitCount = 0;           /* status = W */
                        var returnCount = 0;        /* status = R */
                        var passCount = 0;          /* status = Y */
                        $.each(data, function (idx, item) {                                    
                            
                            var statusWait = "<span class=\"badge badge-primary\" style=\"font-size:100%\">" + item.waitCount + "</span>&nbsp;";
                            var statusReturn = "<span class=\"badge badge-warning\" style=\"font-size:100%\">" + item.returnCount + "</span>&nbsp;";
                            var statusPass = "<span class=\"badge badge-success\" style=\"font-size:100%\">" + item.passCount + "</span>&nbsp;";

                            $('#tblDoc tbody').append(
                                "<tr>" +                                    
                                    "<td class=\"text-center align-middle\">" + statusWait + statusReturn + statusPass + "</td>" +                                    
                                    "<td class=\"align-middle\">" + item.docNameTh + "</td>" +                                                                                                            
                                "</tr>"                                
                            );                            
                        });   

                        $('#doc-wait-count').html(waitCount);
                        $('#doc-return-count').html(returnCount);
                        $('#doc-pass-count').html(passCount);
                    }
                }
            });
        }
    }
</script>
@endsection