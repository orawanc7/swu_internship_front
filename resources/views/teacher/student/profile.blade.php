<div class="card shadow mb-5 rounded card-border-c-blue widget-profile">
    <div class="widget-profile-card-3">
        <img class="img-fluid" src="" alt="Profile-user" id="studentImage">                
    </div>
    <div class="card-body text-center">
        <h3 id="studentName"></h3>
        
        <p class="text-left"><span class="font-weight-bold">เลขประจำตัว</span> <span id="studentId"></span></p>
        <p class="text-left"><span class="font-weight-bold">คณะ</span> <span id="studentDept"></span></p>
        <p class="text-left"><span class="font-weight-bold">สาขา</span> <span id="studentMajor"></span></p>
        <p class="text-left"><span class="font-weight-bold">วิชา</span> <span id="studentCourse"></span></p>
        <p class="text-left"><i class="fas fa-landmark"></i> <span id="studentSchool"></span></p>
        <hr />
        <p class="text-left"><span class="font-weight-bold"><i class="feather icon-mail"></i> Buasri ID</span> <span id="studentBuasriId"></span></p>
        <p class="text-left"><span class="font-weight-bold"><i class="feather icon-mail"></i> Gafe Email</span> <span id="studentGafeEmail"></span></p>
        <p class="text-left"><span class="font-weight-bold"><i class="feather icon-mail"></i> Other Email</span> <span id="studentOtherEmail"></span></p>
        <hr />
        <p class="text-left"><span class="font-weight-bold"><i class="feather icon-phone-call"></i> เบอร์โทร</span> <span id="studentTelephoneNo"></span></p>
        <p class="text-left"><span class="font-weight-bold"><i class="fas fa-mobile-alt"></i> เบอร์มือถือ</span> <span id="studentMobileNo"></span></p>
    </div>
</div>