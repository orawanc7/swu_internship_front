@extends('layout.app')

@section("style")

@endsection

@section("page-header")
<div class="page-header-title">
    <h5 class="m-b-10">รายละเอียดนิสิต</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('') }}"><i class="fa fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="{{ $_SESSION['URL'] }}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="javascript:void();">รายละเอียดนิสิต <span id="roundSemYear"></span></a></li>
</ul>
@endsection

@section('content')
<input type="hidden" name="itsId" id="itsId" value="{{ $_SESSION['ITS_ID'] }}">
<input type="hidden" name="itsStudentId" id="itsStudentId" value="{{ $itsStudentId }}">
<input type="hidden" name="roundId" id="roundId" value="">
<div class="row">
    <div class="col-lg-4">                
        <div class="row">
            <div class="col-12">
                <div class="card shadow mb-5 rounded card-border-c-blue widget-profile">
                    <div class="widget-profile-card-3">
                        <img class="img-fluid" src="" alt="Profile-user" id="studentImage">                
                    </div>
                    <div class="card-body text-center">
                        <h3 id="studentName"></h3>
                        
                        <p class="text-left"><span class="font-weight-bold">เลขประจำตัว</span> <span id="studentId"></span></p>
                        <p class="text-left"><span class="font-weight-bold">คณะ</span> <span id="studentDept"></span></p>
                        <p class="text-left"><span class="font-weight-bold">สาขา</span> <span id="studentMajor"></span></p>
                        <p class="text-left"><span class="font-weight-bold">วิชา</span> <span id="studentCourse"></span></p>
                        <p class="text-left"><i class="fas fa-landmark"></i> <span id="studentSchool"></span></p>
                        <hr />
                        <p class="text-left"><span class="font-weight-bold"><i class="feather icon-mail"></i> Buasri ID</span> <span id="studentBuasriId"></span></p>
                        <p class="text-left"><span class="font-weight-bold"><i class="feather icon-mail"></i> Gafe Email</span> <span id="studentGafeEmail"></span></p>
                        <p class="text-left"><span class="font-weight-bold"><i class="feather icon-mail"></i> Other Email</span> <span id="studentOtherEmail"></span></p>
                        <hr />
                        <p class="text-left"><span class="font-weight-bold"><i class="feather icon-phone-call"></i> เบอร์โทร</span> <span id="studentTelephoneNo"></span></p>
                        <p class="text-left"><span class="font-weight-bold"><i class="fas fa-mobile-alt"></i> เบอร์มือถือ</span> <span id="studentMobileNo"></span></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row d-none" id="div-student-other">
            <div class="col-12">
                <div class="card shadow mb-5 rounded card-border-c-yellow">
                    <div class="card-header">
                        <h5><i class="feather icon-users"></i> นิสิตอื่น</h5>                        
                    </div>
                    <div class="cust-scroll ps ps--active-y">
                        <div class="card-body p-b-0" id="student-other">                            
                            <!-- add here -->                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-8">
        <div class="row">
            <div class="col-lg-12">
<!------------------------------>


<!------------------------------->                
            </div>
        </div>

    </div>
</div>       
@endsection

@section("modal")
@include('profileupload')
@endsection


@section('script')

<script>
    
    $(document).ready(function () {
        var iconPath = "{{ routeApi('Profile/getImageIcon/') }}";    
        var studentPath = "{{ route('teacher/student/') }}";
        TeacherStudent.getInfo(iconPath,studentPath);        
    });

    var TeacherStudent = {        
        getInfo: function(iconPath,studentPath) {
            $.ajax({
                type: "get",
                url: "{{routeApi('SchdStdInfo/get/')}}" + $('#itsStudentId').val(),                
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        var data = response.data;

                        $('#roundId').val(data.roundId);

                        $('#roundSemYear').html("ประจำภาค"+data.semCd + "/" + data.year);
                        $('#studentImage').attr('src', iconPath + data.studentId);
                        
                        $('#studentName').html(data.prenameSnameTh + data.fnameTh + " " + data.lnameTh);
                        $('#studentId').html(data.studentId);
                        $('#studentDept').html(data.deptLnameTh);
                        $('#studentMajor').html(data.majorLnameTh);
                        $('#studentCourse').html(data.courseCd);
                        $('#studentSchool').html(data.schoolNameTh);                        

                        $('#studentBuasriId').html(data.buasrId);   
                        $('#studentGafeEmail').html(data.gafeEmail);   
                        $('#studentOtherEmail').html(data.otherEmail);   
                        $('#studentMobileNo').html(data.mobileNo);   
                        $('#studentTelephoneNo').html(data.telephoneNo);   

                        TeacherStudent.loadOther(iconPath,studentPath);
                    }
                }
            });
        },

        loadOther(iconPath,studentPath) {
            $.ajax({
                type: "get",
                url: "{{routeApi('SchdPersonStd/getStudentByPersonRound')}}",     
                crossDomain:true,           
                dataType: "json",
                data : {
                    'roundId' : $('#roundId').val(),
                    'itsPersonId' : $('#itsId').val()
                },
                success: function (response) {                                        
                    if (response.data) {
                        var data = response.data;       
                        if (data.length>1) {
                            $('#div-student-other').removeClass('d-none');
                        }
                        
                        $.each(data, function (idx, item) {       
                            if (item.itsStudentId!=$('#itsStudentId').val()) {
                                $('#student-other').append(
                                    "<div class=\"align-middle m-b-25\">" +     
                                        "<img src=\"" +   iconPath + item.studentId  +   "\" alt=\"user image\" class=\"img-radius align-top m-r-15\">" + 
                                        "<div class=\"d-inline-block\">" +
                                            "<a href=\"" + studentPath + item.itsStudentId +   "\">" +
                                                "<h6>" + item.prenameSnameTh + item.fnameTh + " " + item.lnameTh + "</h6>" + 
                                            "</a>" + 
                                            "<p class=\"m-b-0\"><i class=\"fas fa-landmark\"></i> " + item.schoolNameTh +"</p>" +                                        
                                        "</div>" +
                                    "</div>"
                                );                            
                            }
                        });                        
                    }
                }
            });
        }
    }
</script>
@endsection