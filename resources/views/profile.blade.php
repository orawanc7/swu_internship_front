@extends('layout.app')

@section("style")
@include('plugin.dropzone.css')
@endsection

@section("page-header")
@endsection

@section('content')


<div class="row">
    <div class="col-lg-3">                
        <div class="card shadow mb-5 rounded card-border-c-blue widget-profile">
            <div class="widget-profile-card-3">
                <img class="img-fluid" src="{{ routeApi('Profile/getImageIcon/' . $_SESSION['ITS_ID']) }}" alt="Profile-user">
                <div class="pt-1">
                    <button class="btn drp-icon btn-rounded btn-outline-primary dropdown-toggle" id="btnChangeImage"><i class="feather icon-camera"></i></button>
                </div>
            </div>
            <div class="card-body text-center">
                <h3>{{ $_SESSION['TITLE'] }}{{ $_SESSION['FIRST_NAME'] }}<br>{{ $_SESSION['LAST_NAME'] }}</h3>
                <p>เลขประจำตัว {{ $_SESSION['ID'] }}</p>
            </div>
        </div>
    </div>

    <div class="col-lg-9">
        <div class="row">
            <div class="col-lg-12">
                <form method="post" action="{{ route('profile/save') }}" id="frmProfile">
                    <div class="card shadow mb-5 rounded card-border-c-blue">
                        <div class="card-header">
                            <i class="feather icon-user"></i> ข้อมูลส่วนตัว
                        </div>
                        <div class="card-body">                
                            <div class="form-group row">
                                <div class="col-lg-3 font-weight-bold">                                    
                                    <label class="col-form-label">คณะ</label>
                                </div>
                                <div class="col-lg-3">
                                    {{ $_SESSION['DEPT'] }}
                                </div>
                                <div class="col-lg-3 font-weight-bold">                                    
                                    <label class="col-form-label">ภาควิชา</label>
                                </div>
                                <div class="col-lg-3">
                                    {{ $_SESSION['INDEPT'] }}
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-3 font-weight-bold">                                    
                                    <label class="col-form-label">ที่อยู่</label>
                                </div>
                                <div class="col-lg-9">
                                    {{ $_SESSION['ADDRESS_DESC'] }}
                                </div>                    
                            </div>
                            <div cla
                            <div class="form-group row">
                                <div class="col-lg-3 font-weight-bold">                                    
                                    <label class="col-form-label">Buasri ID</label>
                                </div>
                                <div class="col-lg-3">
                                    {{ $_SESSION['BUASRI_EMAIL'] }}
                                </div>
                                <div class="col-lg-3 font-weight-bold">                                    
                                    <label class="col-form-label">GAFE Email</label>
                                </div>
                                <div class="col-lg-3">
                                    {{ $_SESSION['GAFE_EMAIL'] }}
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-3 font-weight-bold">                                    
                                    <label class="col-form-label" for="otherEmail">อีเมลอื่น</label>
                                </div>
                                <div class="col-lg-3">                        
                                    <input type="text" class="form-control" value="{{ $_SESSION['OTHER_EMAIL'] }}" autofocus id="otherEmail" name="otherEmail">
                                </div>                    
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-3 font-weight-bold">                                    
                                    <label class="col-form-label" for="telephoneNo">เบอร์โทร</label>
                                </div>
                                <div class="col-lg-3">                        
                                    <input type="text" class="form-control" value="{{ $_SESSION['TELEPHONE_NO'] }}" id="telephoneNo" name="telephoneNo">
                                </div>
                                <div class="col-lg-3 font-weight-bold" for="">                                    
                                    <label class="col-form-label" for="mobileNo">เบอร์มือถือ</label>
                                </div>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control"  value="{{ $_SESSION['MOBILE_NO'] }}" id="mobileNo" name="mobileNo">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <button class="btn btn-sm btn-primary" id="btnSave" type="submit"><i class="feather icon-save"></i> บันทึก</button>
                        </div>
                    </div>  
                </form>
            </div>
        </div>

        
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow mb-5 rounded card-border-c-blue">
                    <div class="card-header">
                        <i class="feather icon-user"></i> การเชื่อมโยง
                    </div>
                    <div class="card-body">                
                        <div class="form-group row">
                            <div class="col-9 font-weight-bold">
                                <img src="@relative("assets/images/line.png")" width="50px;"/> เปิดการแจ้งเตือนผ่าน
                            </div>
                            <div class="col-3 text-right">
                                <div class="switch switch-primary d-inline m-r-10">
                                    <input type="checkbox" id="switch-p-1">
                                    <label for="switch-p-1" class="cr"></label>
                                </div>
                            </div>                    
                        </div>                
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>       
@endsection

@section("modal")
@include('profileupload')
@endsection


@section('script')
@include('plugin.dropzone.js')
<script>
    Dropzone.autoDiscover = false;  
    $(document).ready(function () {
        $('#btnChangeImage').click(function(){
            $("#profile-upload").modal('show');
        });

        $('#frmProfile').submit(function (e) { 
            e.preventDefault();
            
            Profile.save($(this));
        });

        Profile.setupUpload();   
    });

    var Profile = {
        save: function(frm) {            
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        MessageNotify.saveSucess();		
                    } else {
                        MessageNotify.error(response.message);		
                    }
                }
            });
        },

        setupUpload: function() {
            
            $('#profile-upload-form').dropzone({                
                acceptedFiles: "image/*",
                maxFiles : 1,
                addRemoveLinks: true,
                autoProcessQueue: false,
                uploadMultiple: false,
                paramName: 'file',
                init: function() {                    
                    var dropZone = this;
                    $('#btnUpload').click(function(){        
                        dropZone.processQueue();                    
                    });             
                },
                complete: function(file) {                                
                    location.href = "{{ route('profile') }}";
                },
                error: function (file, response) {
                    this.removeFile(file);
                },
            }); 
        } , 
    }
</script>
@endsection