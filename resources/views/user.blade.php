<a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-cog"></i>
</a>
<div class="dropdown-menu dropdown-menu-right profile-notification">
    <div class="pro-head">
        <img src="{{ routeApi('Profile/getImageIcon/' . $_SESSION['ITS_ID']) }}" class="img-radius">
        <span>{{  $_SESSION['NAME'] }}</span>
        <a href="{{ route('logout') }}" class="dud-logout" title="ออกจากระบบ">
            <i class="fa fa-sign-out-alt"></i>
        </a>
    </div>
    <ul class="pro-body">        
        <li><a href="{{ route('profile') }}" class="dropdown-item"><i class="fa fa-user"></i> Profile</a></li>        
    </ul>
</div>