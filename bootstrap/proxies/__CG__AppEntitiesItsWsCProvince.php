<?php

namespace GeneratedProxies\__CG__\App\Entities;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class ItsWsCProvince extends \App\Entities\ItsWsCProvince implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'provinceCd', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'provinceSnameTh', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'provinceLnameTh', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'provinceSnameEng', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'provinceLnameEng', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'creationDtm', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'creationBy', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'lastUpdateDtm', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'lastUpdateBy', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'regionCd'];
        }

        return ['__isInitialized__', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'provinceCd', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'provinceSnameTh', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'provinceLnameTh', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'provinceSnameEng', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'provinceLnameEng', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'creationDtm', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'creationBy', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'lastUpdateDtm', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'lastUpdateBy', '' . "\0" . 'App\\Entities\\ItsWsCProvince' . "\0" . 'regionCd'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (ItsWsCProvince $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getProvinceCd()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getProvinceCd();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getProvinceCd', []);

        return parent::getProvinceCd();
    }

    /**
     * {@inheritDoc}
     */
    public function setProvinceSnameTh($provinceSnameTh = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setProvinceSnameTh', [$provinceSnameTh]);

        return parent::setProvinceSnameTh($provinceSnameTh);
    }

    /**
     * {@inheritDoc}
     */
    public function getProvinceSnameTh()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getProvinceSnameTh', []);

        return parent::getProvinceSnameTh();
    }

    /**
     * {@inheritDoc}
     */
    public function setProvinceLnameTh($provinceLnameTh = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setProvinceLnameTh', [$provinceLnameTh]);

        return parent::setProvinceLnameTh($provinceLnameTh);
    }

    /**
     * {@inheritDoc}
     */
    public function getProvinceLnameTh()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getProvinceLnameTh', []);

        return parent::getProvinceLnameTh();
    }

    /**
     * {@inheritDoc}
     */
    public function setProvinceSnameEng($provinceSnameEng = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setProvinceSnameEng', [$provinceSnameEng]);

        return parent::setProvinceSnameEng($provinceSnameEng);
    }

    /**
     * {@inheritDoc}
     */
    public function getProvinceSnameEng()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getProvinceSnameEng', []);

        return parent::getProvinceSnameEng();
    }

    /**
     * {@inheritDoc}
     */
    public function setProvinceLnameEng($provinceLnameEng = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setProvinceLnameEng', [$provinceLnameEng]);

        return parent::setProvinceLnameEng($provinceLnameEng);
    }

    /**
     * {@inheritDoc}
     */
    public function getProvinceLnameEng()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getProvinceLnameEng', []);

        return parent::getProvinceLnameEng();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreationDtm($creationDtm = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreationDtm', [$creationDtm]);

        return parent::setCreationDtm($creationDtm);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreationDtm()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreationDtm', []);

        return parent::getCreationDtm();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreationBy($creationBy = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreationBy', [$creationBy]);

        return parent::setCreationBy($creationBy);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreationBy()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreationBy', []);

        return parent::getCreationBy();
    }

    /**
     * {@inheritDoc}
     */
    public function setLastUpdateDtm($lastUpdateDtm = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLastUpdateDtm', [$lastUpdateDtm]);

        return parent::setLastUpdateDtm($lastUpdateDtm);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastUpdateDtm()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLastUpdateDtm', []);

        return parent::getLastUpdateDtm();
    }

    /**
     * {@inheritDoc}
     */
    public function setLastUpdateBy($lastUpdateBy = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLastUpdateBy', [$lastUpdateBy]);

        return parent::setLastUpdateBy($lastUpdateBy);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastUpdateBy()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLastUpdateBy', []);

        return parent::getLastUpdateBy();
    }

    /**
     * {@inheritDoc}
     */
    public function setRegionCd(\App\Entities\ItsWsCRegion $regionCd = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRegionCd', [$regionCd]);

        return parent::setRegionCd($regionCd);
    }

    /**
     * {@inheritDoc}
     */
    public function getRegionCd()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRegionCd', []);

        return parent::getRegionCd();
    }

}
