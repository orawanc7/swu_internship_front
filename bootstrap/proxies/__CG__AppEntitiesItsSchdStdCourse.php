<?php

namespace GeneratedProxies\__CG__\App\Entities;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class ItsSchdStdCourse extends \App\Entities\ItsSchdStdCourse implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'stdCourseId', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'itsCourseCd', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'itsCourseName', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'levelClass', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'creditAmt', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'minutesQty', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'hourAmt', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'stdRemark', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'sendDate', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'remark', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'activeFlag', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'creationDtm', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'creationBy', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'lastUpdateDtm', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'lastUpdateBy', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'programCd', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'itsRound', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'schoolDtl', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'itsStudent'];
        }

        return ['__isInitialized__', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'stdCourseId', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'itsCourseCd', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'itsCourseName', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'levelClass', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'creditAmt', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'minutesQty', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'hourAmt', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'stdRemark', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'sendDate', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'remark', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'activeFlag', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'creationDtm', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'creationBy', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'lastUpdateDtm', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'lastUpdateBy', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'programCd', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'itsRound', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'schoolDtl', '' . "\0" . 'App\\Entities\\ItsSchdStdCourse' . "\0" . 'itsStudent'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (ItsSchdStdCourse $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function setStdCourseId($stdCourseId = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStdCourseId', [$stdCourseId]);

        return parent::setStdCourseId($stdCourseId);
    }

    /**
     * {@inheritDoc}
     */
    public function getStdCourseId()
    {
        if ($this->__isInitialized__ === false) {
            return  parent::getStdCourseId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStdCourseId', []);

        return parent::getStdCourseId();
    }

    /**
     * {@inheritDoc}
     */
    public function setItsCourseCd($itsCourseCd = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setItsCourseCd', [$itsCourseCd]);

        return parent::setItsCourseCd($itsCourseCd);
    }

    /**
     * {@inheritDoc}
     */
    public function getItsCourseCd()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getItsCourseCd', []);

        return parent::getItsCourseCd();
    }

    /**
     * {@inheritDoc}
     */
    public function setItsCourseName($itsCourseName = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setItsCourseName', [$itsCourseName]);

        return parent::setItsCourseName($itsCourseName);
    }

    /**
     * {@inheritDoc}
     */
    public function getItsCourseName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getItsCourseName', []);

        return parent::getItsCourseName();
    }

    /**
     * {@inheritDoc}
     */
    public function setLevelClass($levelClass = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLevelClass', [$levelClass]);

        return parent::setLevelClass($levelClass);
    }

    /**
     * {@inheritDoc}
     */
    public function getLevelClass()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLevelClass', []);

        return parent::getLevelClass();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreditAmt($creditAmt = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreditAmt', [$creditAmt]);

        return parent::setCreditAmt($creditAmt);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreditAmt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreditAmt', []);

        return parent::getCreditAmt();
    }

    /**
     * {@inheritDoc}
     */
    public function setMinutesQty($minutesQty = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMinutesQty', [$minutesQty]);

        return parent::setMinutesQty($minutesQty);
    }

    /**
     * {@inheritDoc}
     */
    public function getMinutesQty()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMinutesQty', []);

        return parent::getMinutesQty();
    }

    /**
     * {@inheritDoc}
     */
    public function setHourAmt($hourAmt = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setHourAmt', [$hourAmt]);

        return parent::setHourAmt($hourAmt);
    }

    /**
     * {@inheritDoc}
     */
    public function getHourAmt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHourAmt', []);

        return parent::getHourAmt();
    }

    /**
     * {@inheritDoc}
     */
    public function setStdRemark($stdRemark = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStdRemark', [$stdRemark]);

        return parent::setStdRemark($stdRemark);
    }

    /**
     * {@inheritDoc}
     */
    public function getStdRemark()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStdRemark', []);

        return parent::getStdRemark();
    }

    /**
     * {@inheritDoc}
     */
    public function setSendDate($sendDate = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSendDate', [$sendDate]);

        return parent::setSendDate($sendDate);
    }

    /**
     * {@inheritDoc}
     */
    public function getSendDate()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSendDate', []);

        return parent::getSendDate();
    }

    /**
     * {@inheritDoc}
     */
    public function setRemark($remark = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRemark', [$remark]);

        return parent::setRemark($remark);
    }

    /**
     * {@inheritDoc}
     */
    public function getRemark()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRemark', []);

        return parent::getRemark();
    }

    /**
     * {@inheritDoc}
     */
    public function setActiveFlag($activeFlag = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setActiveFlag', [$activeFlag]);

        return parent::setActiveFlag($activeFlag);
    }

    /**
     * {@inheritDoc}
     */
    public function getActiveFlag()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getActiveFlag', []);

        return parent::getActiveFlag();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreationDtm($creationDtm = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreationDtm', [$creationDtm]);

        return parent::setCreationDtm($creationDtm);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreationDtm()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreationDtm', []);

        return parent::getCreationDtm();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreationBy($creationBy = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreationBy', [$creationBy]);

        return parent::setCreationBy($creationBy);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreationBy()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreationBy', []);

        return parent::getCreationBy();
    }

    /**
     * {@inheritDoc}
     */
    public function setLastUpdateDtm($lastUpdateDtm = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLastUpdateDtm', [$lastUpdateDtm]);

        return parent::setLastUpdateDtm($lastUpdateDtm);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastUpdateDtm()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLastUpdateDtm', []);

        return parent::getLastUpdateDtm();
    }

    /**
     * {@inheritDoc}
     */
    public function setLastUpdateBy($lastUpdateBy = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLastUpdateBy', [$lastUpdateBy]);

        return parent::setLastUpdateBy($lastUpdateBy);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastUpdateBy()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLastUpdateBy', []);

        return parent::getLastUpdateBy();
    }

    /**
     * {@inheritDoc}
     */
    public function setProgramCd($programCd = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setProgramCd', [$programCd]);

        return parent::setProgramCd($programCd);
    }

    /**
     * {@inheritDoc}
     */
    public function getProgramCd()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getProgramCd', []);

        return parent::getProgramCd();
    }

    /**
     * {@inheritDoc}
     */
    public function setItsRound(\App\Entities\ItsSchdSemYear $itsRound = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setItsRound', [$itsRound]);

        return parent::setItsRound($itsRound);
    }

    /**
     * {@inheritDoc}
     */
    public function getItsRound()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getItsRound', []);

        return parent::getItsRound();
    }

    /**
     * {@inheritDoc}
     */
    public function setSchoolDtl(\App\Entities\ItsSchdSchoolDtl $schoolDtl = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSchoolDtl', [$schoolDtl]);

        return parent::setSchoolDtl($schoolDtl);
    }

    /**
     * {@inheritDoc}
     */
    public function getSchoolDtl()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSchoolDtl', []);

        return parent::getSchoolDtl();
    }

    /**
     * {@inheritDoc}
     */
    public function setItsStudent(\App\Entities\ItsSchdStdInfo $itsStudent = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setItsStudent', [$itsStudent]);

        return parent::setItsStudent($itsStudent);
    }

    /**
     * {@inheritDoc}
     */
    public function getItsStudent()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getItsStudent', []);

        return parent::getItsStudent();
    }

    /**
     * {@inheritDoc}
     */
    public function getKeyCd()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getKeyCd', []);

        return parent::getKeyCd();
    }

    /**
     * {@inheritDoc}
     */
    public function getKeyGen()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getKeyGen', []);

        return parent::getKeyGen();
    }

}
