<?php

namespace GeneratedProxies\__CG__\App\Entities;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class ItsSchdNotification extends \App\Entities\ItsSchdNotification implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'itsNotifyId', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'notifyType', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'urlLink', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'senderId', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'senderMessage', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'receiverId', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'receiverMessage', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'readFlag', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'creationDtm', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'creationBy', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'lastUpdateDtm', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'lastUpdateBy', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'programCd', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'receiverType', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'senderType'];
        }

        return ['__isInitialized__', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'itsNotifyId', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'notifyType', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'urlLink', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'senderId', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'senderMessage', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'receiverId', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'receiverMessage', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'readFlag', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'creationDtm', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'creationBy', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'lastUpdateDtm', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'lastUpdateBy', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'programCd', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'receiverType', '' . "\0" . 'App\\Entities\\ItsSchdNotification' . "\0" . 'senderType'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (ItsSchdNotification $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getItsNotifyId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getItsNotifyId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getItsNotifyId', []);

        return parent::getItsNotifyId();
    }

    /**
     * {@inheritDoc}
     */
    public function setNotifyType($notifyType = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNotifyType', [$notifyType]);

        return parent::setNotifyType($notifyType);
    }

    /**
     * {@inheritDoc}
     */
    public function getNotifyType()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNotifyType', []);

        return parent::getNotifyType();
    }

    /**
     * {@inheritDoc}
     */
    public function setUrlLink($urlLink = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUrlLink', [$urlLink]);

        return parent::setUrlLink($urlLink);
    }

    /**
     * {@inheritDoc}
     */
    public function getUrlLink()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUrlLink', []);

        return parent::getUrlLink();
    }

    /**
     * {@inheritDoc}
     */
    public function setSenderId($senderId = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSenderId', [$senderId]);

        return parent::setSenderId($senderId);
    }

    /**
     * {@inheritDoc}
     */
    public function getSenderId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSenderId', []);

        return parent::getSenderId();
    }

    /**
     * {@inheritDoc}
     */
    public function setSenderMessage($senderMessage = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSenderMessage', [$senderMessage]);

        return parent::setSenderMessage($senderMessage);
    }

    /**
     * {@inheritDoc}
     */
    public function getSenderMessage()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSenderMessage', []);

        return parent::getSenderMessage();
    }

    /**
     * {@inheritDoc}
     */
    public function setReceiverId($receiverId = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setReceiverId', [$receiverId]);

        return parent::setReceiverId($receiverId);
    }

    /**
     * {@inheritDoc}
     */
    public function getReceiverId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getReceiverId', []);

        return parent::getReceiverId();
    }

    /**
     * {@inheritDoc}
     */
    public function setReceiverMessage($receiverMessage = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setReceiverMessage', [$receiverMessage]);

        return parent::setReceiverMessage($receiverMessage);
    }

    /**
     * {@inheritDoc}
     */
    public function getReceiverMessage()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getReceiverMessage', []);

        return parent::getReceiverMessage();
    }

    /**
     * {@inheritDoc}
     */
    public function setReadFlag($readFlag = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setReadFlag', [$readFlag]);

        return parent::setReadFlag($readFlag);
    }

    /**
     * {@inheritDoc}
     */
    public function getReadFlag()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getReadFlag', []);

        return parent::getReadFlag();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreationDtm($creationDtm = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreationDtm', [$creationDtm]);

        return parent::setCreationDtm($creationDtm);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreationDtm()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreationDtm', []);

        return parent::getCreationDtm();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreationBy($creationBy = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreationBy', [$creationBy]);

        return parent::setCreationBy($creationBy);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreationBy()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreationBy', []);

        return parent::getCreationBy();
    }

    /**
     * {@inheritDoc}
     */
    public function setLastUpdateDtm($lastUpdateDtm = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLastUpdateDtm', [$lastUpdateDtm]);

        return parent::setLastUpdateDtm($lastUpdateDtm);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastUpdateDtm()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLastUpdateDtm', []);

        return parent::getLastUpdateDtm();
    }

    /**
     * {@inheritDoc}
     */
    public function setLastUpdateBy($lastUpdateBy = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLastUpdateBy', [$lastUpdateBy]);

        return parent::setLastUpdateBy($lastUpdateBy);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastUpdateBy()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLastUpdateBy', []);

        return parent::getLastUpdateBy();
    }

    /**
     * {@inheritDoc}
     */
    public function setProgramCd($programCd = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setProgramCd', [$programCd]);

        return parent::setProgramCd($programCd);
    }

    /**
     * {@inheritDoc}
     */
    public function getProgramCd()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getProgramCd', []);

        return parent::getProgramCd();
    }

    /**
     * {@inheritDoc}
     */
    public function setReceiverType(\App\Entities\ItsCPersonType $receiverType = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setReceiverType', [$receiverType]);

        return parent::setReceiverType($receiverType);
    }

    /**
     * {@inheritDoc}
     */
    public function getReceiverType()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getReceiverType', []);

        return parent::getReceiverType();
    }

    /**
     * {@inheritDoc}
     */
    public function setSenderType(\App\Entities\ItsCPersonType $senderType = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSenderType', [$senderType]);

        return parent::setSenderType($senderType);
    }

    /**
     * {@inheritDoc}
     */
    public function getSenderType()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSenderType', []);

        return parent::getSenderType();
    }

}
