<?php

namespace GeneratedProxies\__CG__\App\Entities;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class ItsSchdStdInfoHist extends \App\Entities\ItsSchdStdInfoHist implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'itsStudentId', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'courseCd', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'courseNo', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'leaderFlag', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'languageDoc', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'itsStdDocType', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'calFullScoreAmt', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'userGrade', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'seminarAmt', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'expiredDate', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'remark', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'activeFlag', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'creationDtm', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'creationBy', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'lastUpdateDtm', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'lastUpdateBy', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'programCd', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'transferDate', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'calGrade', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'itsMajor', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'itsRound', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'student'];
        }

        return ['__isInitialized__', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'itsStudentId', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'courseCd', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'courseNo', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'leaderFlag', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'languageDoc', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'itsStdDocType', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'calFullScoreAmt', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'userGrade', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'seminarAmt', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'expiredDate', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'remark', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'activeFlag', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'creationDtm', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'creationBy', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'lastUpdateDtm', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'lastUpdateBy', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'programCd', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'transferDate', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'calGrade', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'itsMajor', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'itsRound', '' . "\0" . 'App\\Entities\\ItsSchdStdInfoHist' . "\0" . 'student'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (ItsSchdStdInfoHist $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getItsStudentId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getItsStudentId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getItsStudentId', []);

        return parent::getItsStudentId();
    }

    /**
     * {@inheritDoc}
     */
    public function setCourseCd($courseCd = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCourseCd', [$courseCd]);

        return parent::setCourseCd($courseCd);
    }

    /**
     * {@inheritDoc}
     */
    public function getCourseCd()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCourseCd', []);

        return parent::getCourseCd();
    }

    /**
     * {@inheritDoc}
     */
    public function setCourseNo($courseNo = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCourseNo', [$courseNo]);

        return parent::setCourseNo($courseNo);
    }

    /**
     * {@inheritDoc}
     */
    public function getCourseNo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCourseNo', []);

        return parent::getCourseNo();
    }

    /**
     * {@inheritDoc}
     */
    public function setLeaderFlag($leaderFlag = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLeaderFlag', [$leaderFlag]);

        return parent::setLeaderFlag($leaderFlag);
    }

    /**
     * {@inheritDoc}
     */
    public function getLeaderFlag()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLeaderFlag', []);

        return parent::getLeaderFlag();
    }

    /**
     * {@inheritDoc}
     */
    public function setLanguageDoc($languageDoc = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLanguageDoc', [$languageDoc]);

        return parent::setLanguageDoc($languageDoc);
    }

    /**
     * {@inheritDoc}
     */
    public function getLanguageDoc()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLanguageDoc', []);

        return parent::getLanguageDoc();
    }

    /**
     * {@inheritDoc}
     */
    public function setItsStdDocType($itsStdDocType = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setItsStdDocType', [$itsStdDocType]);

        return parent::setItsStdDocType($itsStdDocType);
    }

    /**
     * {@inheritDoc}
     */
    public function getItsStdDocType()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getItsStdDocType', []);

        return parent::getItsStdDocType();
    }

    /**
     * {@inheritDoc}
     */
    public function setCalFullScoreAmt($calFullScoreAmt = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCalFullScoreAmt', [$calFullScoreAmt]);

        return parent::setCalFullScoreAmt($calFullScoreAmt);
    }

    /**
     * {@inheritDoc}
     */
    public function getCalFullScoreAmt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCalFullScoreAmt', []);

        return parent::getCalFullScoreAmt();
    }

    /**
     * {@inheritDoc}
     */
    public function setUserGrade($userGrade = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUserGrade', [$userGrade]);

        return parent::setUserGrade($userGrade);
    }

    /**
     * {@inheritDoc}
     */
    public function getUserGrade()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUserGrade', []);

        return parent::getUserGrade();
    }

    /**
     * {@inheritDoc}
     */
    public function setSeminarAmt($seminarAmt = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSeminarAmt', [$seminarAmt]);

        return parent::setSeminarAmt($seminarAmt);
    }

    /**
     * {@inheritDoc}
     */
    public function getSeminarAmt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSeminarAmt', []);

        return parent::getSeminarAmt();
    }

    /**
     * {@inheritDoc}
     */
    public function setExpiredDate($expiredDate = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setExpiredDate', [$expiredDate]);

        return parent::setExpiredDate($expiredDate);
    }

    /**
     * {@inheritDoc}
     */
    public function getExpiredDate()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getExpiredDate', []);

        return parent::getExpiredDate();
    }

    /**
     * {@inheritDoc}
     */
    public function setRemark($remark = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRemark', [$remark]);

        return parent::setRemark($remark);
    }

    /**
     * {@inheritDoc}
     */
    public function getRemark()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRemark', []);

        return parent::getRemark();
    }

    /**
     * {@inheritDoc}
     */
    public function setActiveFlag($activeFlag = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setActiveFlag', [$activeFlag]);

        return parent::setActiveFlag($activeFlag);
    }

    /**
     * {@inheritDoc}
     */
    public function getActiveFlag()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getActiveFlag', []);

        return parent::getActiveFlag();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreationDtm($creationDtm = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreationDtm', [$creationDtm]);

        return parent::setCreationDtm($creationDtm);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreationDtm()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreationDtm', []);

        return parent::getCreationDtm();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreationBy($creationBy = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreationBy', [$creationBy]);

        return parent::setCreationBy($creationBy);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreationBy()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreationBy', []);

        return parent::getCreationBy();
    }

    /**
     * {@inheritDoc}
     */
    public function setLastUpdateDtm($lastUpdateDtm = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLastUpdateDtm', [$lastUpdateDtm]);

        return parent::setLastUpdateDtm($lastUpdateDtm);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastUpdateDtm()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLastUpdateDtm', []);

        return parent::getLastUpdateDtm();
    }

    /**
     * {@inheritDoc}
     */
    public function setLastUpdateBy($lastUpdateBy = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLastUpdateBy', [$lastUpdateBy]);

        return parent::setLastUpdateBy($lastUpdateBy);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastUpdateBy()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLastUpdateBy', []);

        return parent::getLastUpdateBy();
    }

    /**
     * {@inheritDoc}
     */
    public function setProgramCd($programCd = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setProgramCd', [$programCd]);

        return parent::setProgramCd($programCd);
    }

    /**
     * {@inheritDoc}
     */
    public function getProgramCd()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getProgramCd', []);

        return parent::getProgramCd();
    }

    /**
     * {@inheritDoc}
     */
    public function setTransferDate($transferDate = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTransferDate', [$transferDate]);

        return parent::setTransferDate($transferDate);
    }

    /**
     * {@inheritDoc}
     */
    public function getTransferDate()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTransferDate', []);

        return parent::getTransferDate();
    }

    /**
     * {@inheritDoc}
     */
    public function setCalGrade(\App\Entities\ItsSchdGradeSetup $calGrade = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCalGrade', [$calGrade]);

        return parent::setCalGrade($calGrade);
    }

    /**
     * {@inheritDoc}
     */
    public function getCalGrade()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCalGrade', []);

        return parent::getCalGrade();
    }

    /**
     * {@inheritDoc}
     */
    public function setItsMajor(\App\Entities\ItsCMajorMaster $itsMajor = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setItsMajor', [$itsMajor]);

        return parent::setItsMajor($itsMajor);
    }

    /**
     * {@inheritDoc}
     */
    public function getItsMajor()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getItsMajor', []);

        return parent::getItsMajor();
    }

    /**
     * {@inheritDoc}
     */
    public function setItsRound(\App\Entities\ItsSchdSemYear $itsRound = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setItsRound', [$itsRound]);

        return parent::setItsRound($itsRound);
    }

    /**
     * {@inheritDoc}
     */
    public function getItsRound()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getItsRound', []);

        return parent::getItsRound();
    }

    /**
     * {@inheritDoc}
     */
    public function setStudent(\App\Entities\ItsStdMaster $student = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStudent', [$student]);

        return parent::setStudent($student);
    }

    /**
     * {@inheritDoc}
     */
    public function getStudent()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStudent', []);

        return parent::getStudent();
    }

}
