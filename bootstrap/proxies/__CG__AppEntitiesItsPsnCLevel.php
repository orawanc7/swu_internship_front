<?php

namespace GeneratedProxies\__CG__\App\Entities;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class ItsPsnCLevel extends \App\Entities\ItsPsnCLevel implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'levelCd', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'levelSnameTh', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'levelLnameTh', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'levelSnameEng', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'levelLnameEng', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'seqLevel', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'activeFlag', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'remark', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'creationDtm', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'creationBy', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'lastUpdateDtm', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'lastUpdateBy', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'programCd'];
        }

        return ['__isInitialized__', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'levelCd', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'levelSnameTh', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'levelLnameTh', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'levelSnameEng', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'levelLnameEng', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'seqLevel', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'activeFlag', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'remark', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'creationDtm', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'creationBy', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'lastUpdateDtm', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'lastUpdateBy', '' . "\0" . 'App\\Entities\\ItsPsnCLevel' . "\0" . 'programCd'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (ItsPsnCLevel $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getLevelCd()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getLevelCd();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLevelCd', []);

        return parent::getLevelCd();
    }

    /**
     * {@inheritDoc}
     */
    public function setLevelSnameTh($levelSnameTh = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLevelSnameTh', [$levelSnameTh]);

        return parent::setLevelSnameTh($levelSnameTh);
    }

    /**
     * {@inheritDoc}
     */
    public function getLevelSnameTh()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLevelSnameTh', []);

        return parent::getLevelSnameTh();
    }

    /**
     * {@inheritDoc}
     */
    public function setLevelLnameTh($levelLnameTh = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLevelLnameTh', [$levelLnameTh]);

        return parent::setLevelLnameTh($levelLnameTh);
    }

    /**
     * {@inheritDoc}
     */
    public function getLevelLnameTh()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLevelLnameTh', []);

        return parent::getLevelLnameTh();
    }

    /**
     * {@inheritDoc}
     */
    public function setLevelSnameEng($levelSnameEng = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLevelSnameEng', [$levelSnameEng]);

        return parent::setLevelSnameEng($levelSnameEng);
    }

    /**
     * {@inheritDoc}
     */
    public function getLevelSnameEng()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLevelSnameEng', []);

        return parent::getLevelSnameEng();
    }

    /**
     * {@inheritDoc}
     */
    public function setLevelLnameEng($levelLnameEng = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLevelLnameEng', [$levelLnameEng]);

        return parent::setLevelLnameEng($levelLnameEng);
    }

    /**
     * {@inheritDoc}
     */
    public function getLevelLnameEng()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLevelLnameEng', []);

        return parent::getLevelLnameEng();
    }

    /**
     * {@inheritDoc}
     */
    public function setSeqLevel($seqLevel = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSeqLevel', [$seqLevel]);

        return parent::setSeqLevel($seqLevel);
    }

    /**
     * {@inheritDoc}
     */
    public function getSeqLevel()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSeqLevel', []);

        return parent::getSeqLevel();
    }

    /**
     * {@inheritDoc}
     */
    public function setActiveFlag($activeFlag = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setActiveFlag', [$activeFlag]);

        return parent::setActiveFlag($activeFlag);
    }

    /**
     * {@inheritDoc}
     */
    public function getActiveFlag()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getActiveFlag', []);

        return parent::getActiveFlag();
    }

    /**
     * {@inheritDoc}
     */
    public function setRemark($remark = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRemark', [$remark]);

        return parent::setRemark($remark);
    }

    /**
     * {@inheritDoc}
     */
    public function getRemark()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRemark', []);

        return parent::getRemark();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreationDtm($creationDtm = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreationDtm', [$creationDtm]);

        return parent::setCreationDtm($creationDtm);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreationDtm()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreationDtm', []);

        return parent::getCreationDtm();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreationBy($creationBy = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreationBy', [$creationBy]);

        return parent::setCreationBy($creationBy);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreationBy()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreationBy', []);

        return parent::getCreationBy();
    }

    /**
     * {@inheritDoc}
     */
    public function setLastUpdateDtm($lastUpdateDtm = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLastUpdateDtm', [$lastUpdateDtm]);

        return parent::setLastUpdateDtm($lastUpdateDtm);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastUpdateDtm()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLastUpdateDtm', []);

        return parent::getLastUpdateDtm();
    }

    /**
     * {@inheritDoc}
     */
    public function setLastUpdateBy($lastUpdateBy = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLastUpdateBy', [$lastUpdateBy]);

        return parent::setLastUpdateBy($lastUpdateBy);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastUpdateBy()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLastUpdateBy', []);

        return parent::getLastUpdateBy();
    }

    /**
     * {@inheritDoc}
     */
    public function setProgramCd($programCd = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setProgramCd', [$programCd]);

        return parent::setProgramCd($programCd);
    }

    /**
     * {@inheritDoc}
     */
    public function getProgramCd()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getProgramCd', []);

        return parent::getProgramCd();
    }

}
