<?php

namespace GeneratedProxies\__CG__\App\Entities;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class ItsFrmAnswer extends \App\Entities\ItsFrmAnswer implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'ansId', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'ansSeq', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'ansDescTh', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'ansDescEn', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'ansExplanTh', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'ansExplanEn', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'size1', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'size2', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'ansCorrectFlag', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'weightedValue', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'rawScoreAmt', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'remark', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'activeFlag', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'creationDtm', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'creationBy', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'lastUpdateDtm', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'lastUpdateBy', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'programCd', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'frmQues', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'contron', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'refFrmQues', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'refSection'];
        }

        return ['__isInitialized__', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'ansId', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'ansSeq', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'ansDescTh', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'ansDescEn', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'ansExplanTh', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'ansExplanEn', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'size1', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'size2', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'ansCorrectFlag', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'weightedValue', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'rawScoreAmt', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'remark', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'activeFlag', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'creationDtm', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'creationBy', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'lastUpdateDtm', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'lastUpdateBy', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'programCd', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'frmQues', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'contron', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'refFrmQues', '' . "\0" . 'App\\Entities\\ItsFrmAnswer' . "\0" . 'refSection'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (ItsFrmAnswer $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getAnsId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getAnsId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAnsId', []);

        return parent::getAnsId();
    }

    /**
     * {@inheritDoc}
     */
    public function setAnsSeq($ansSeq = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAnsSeq', [$ansSeq]);

        return parent::setAnsSeq($ansSeq);
    }

    /**
     * {@inheritDoc}
     */
    public function getAnsSeq()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAnsSeq', []);

        return parent::getAnsSeq();
    }

    /**
     * {@inheritDoc}
     */
    public function setAnsDescTh($ansDescTh = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAnsDescTh', [$ansDescTh]);

        return parent::setAnsDescTh($ansDescTh);
    }

    /**
     * {@inheritDoc}
     */
    public function getAnsDescTh()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAnsDescTh', []);

        return parent::getAnsDescTh();
    }

    /**
     * {@inheritDoc}
     */
    public function setAnsDescEn($ansDescEn = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAnsDescEn', [$ansDescEn]);

        return parent::setAnsDescEn($ansDescEn);
    }

    /**
     * {@inheritDoc}
     */
    public function getAnsDescEn()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAnsDescEn', []);

        return parent::getAnsDescEn();
    }

    /**
     * {@inheritDoc}
     */
    public function setAnsExplanTh($ansExplanTh = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAnsExplanTh', [$ansExplanTh]);

        return parent::setAnsExplanTh($ansExplanTh);
    }

    /**
     * {@inheritDoc}
     */
    public function getAnsExplanTh()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAnsExplanTh', []);

        return parent::getAnsExplanTh();
    }

    /**
     * {@inheritDoc}
     */
    public function setAnsExplanEn($ansExplanEn = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAnsExplanEn', [$ansExplanEn]);

        return parent::setAnsExplanEn($ansExplanEn);
    }

    /**
     * {@inheritDoc}
     */
    public function getAnsExplanEn()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAnsExplanEn', []);

        return parent::getAnsExplanEn();
    }

    /**
     * {@inheritDoc}
     */
    public function setSize1($size1 = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSize1', [$size1]);

        return parent::setSize1($size1);
    }

    /**
     * {@inheritDoc}
     */
    public function getSize1()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSize1', []);

        return parent::getSize1();
    }

    /**
     * {@inheritDoc}
     */
    public function setSize2($size2 = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSize2', [$size2]);

        return parent::setSize2($size2);
    }

    /**
     * {@inheritDoc}
     */
    public function getSize2()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSize2', []);

        return parent::getSize2();
    }

    /**
     * {@inheritDoc}
     */
    public function setAnsCorrectFlag($ansCorrectFlag = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAnsCorrectFlag', [$ansCorrectFlag]);

        return parent::setAnsCorrectFlag($ansCorrectFlag);
    }

    /**
     * {@inheritDoc}
     */
    public function getAnsCorrectFlag()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAnsCorrectFlag', []);

        return parent::getAnsCorrectFlag();
    }

    /**
     * {@inheritDoc}
     */
    public function setWeightedValue($weightedValue = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setWeightedValue', [$weightedValue]);

        return parent::setWeightedValue($weightedValue);
    }

    /**
     * {@inheritDoc}
     */
    public function getWeightedValue()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getWeightedValue', []);

        return parent::getWeightedValue();
    }

    /**
     * {@inheritDoc}
     */
    public function setRawScoreAmt($rawScoreAmt = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRawScoreAmt', [$rawScoreAmt]);

        return parent::setRawScoreAmt($rawScoreAmt);
    }

    /**
     * {@inheritDoc}
     */
    public function getRawScoreAmt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRawScoreAmt', []);

        return parent::getRawScoreAmt();
    }

    /**
     * {@inheritDoc}
     */
    public function setRemark($remark = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRemark', [$remark]);

        return parent::setRemark($remark);
    }

    /**
     * {@inheritDoc}
     */
    public function getRemark()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRemark', []);

        return parent::getRemark();
    }

    /**
     * {@inheritDoc}
     */
    public function setActiveFlag($activeFlag = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setActiveFlag', [$activeFlag]);

        return parent::setActiveFlag($activeFlag);
    }

    /**
     * {@inheritDoc}
     */
    public function getActiveFlag()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getActiveFlag', []);

        return parent::getActiveFlag();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreationDtm($creationDtm = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreationDtm', [$creationDtm]);

        return parent::setCreationDtm($creationDtm);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreationDtm()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreationDtm', []);

        return parent::getCreationDtm();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreationBy($creationBy = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreationBy', [$creationBy]);

        return parent::setCreationBy($creationBy);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreationBy()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreationBy', []);

        return parent::getCreationBy();
    }

    /**
     * {@inheritDoc}
     */
    public function setLastUpdateDtm($lastUpdateDtm = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLastUpdateDtm', [$lastUpdateDtm]);

        return parent::setLastUpdateDtm($lastUpdateDtm);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastUpdateDtm()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLastUpdateDtm', []);

        return parent::getLastUpdateDtm();
    }

    /**
     * {@inheritDoc}
     */
    public function setLastUpdateBy($lastUpdateBy = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLastUpdateBy', [$lastUpdateBy]);

        return parent::setLastUpdateBy($lastUpdateBy);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastUpdateBy()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLastUpdateBy', []);

        return parent::getLastUpdateBy();
    }

    /**
     * {@inheritDoc}
     */
    public function setProgramCd($programCd = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setProgramCd', [$programCd]);

        return parent::setProgramCd($programCd);
    }

    /**
     * {@inheritDoc}
     */
    public function getProgramCd()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getProgramCd', []);

        return parent::getProgramCd();
    }

    /**
     * {@inheritDoc}
     */
    public function setFrmQues(\App\Entities\ItsFrmQuestion $frmQues = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFrmQues', [$frmQues]);

        return parent::setFrmQues($frmQues);
    }

    /**
     * {@inheritDoc}
     */
    public function getFrmQues()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFrmQues', []);

        return parent::getFrmQues();
    }

    /**
     * {@inheritDoc}
     */
    public function setContron(\App\Entities\ItsCControlType $contron = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setContron', [$contron]);

        return parent::setContron($contron);
    }

    /**
     * {@inheritDoc}
     */
    public function getContron()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getContron', []);

        return parent::getContron();
    }

    /**
     * {@inheritDoc}
     */
    public function setRefFrmQues(\App\Entities\ItsFrmQuestion $refFrmQues = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRefFrmQues', [$refFrmQues]);

        return parent::setRefFrmQues($refFrmQues);
    }

    /**
     * {@inheritDoc}
     */
    public function getRefFrmQues()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRefFrmQues', []);

        return parent::getRefFrmQues();
    }

    /**
     * {@inheritDoc}
     */
    public function setRefSection(\App\Entities\ItsFrmSection $refSection = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRefSection', [$refSection]);

        return parent::setRefSection($refSection);
    }

    /**
     * {@inheritDoc}
     */
    public function getRefSection()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRefSection', []);

        return parent::getRefSection();
    }

}
